
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pernix.incae.Application;
import com.pernix.incae.data.TeamMemberRepository;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.TeamMember;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class TeamMemberServicesTest extends BaseTest {

    private static final String PATH = "/teamMembers";
    private static final String NAME = "Team member name";
    private static final int NOT_FOUND_ID = 16357;

    @Autowired
    private TeamMemberRepository teamMemberRepository;

    private TeamMember teamMember;
    private Game game;

    @Before
    public void prepareTestMethod() throws Exception {
        game = createGame();
        teamMember = createTeamMember(game);
    }

    private TeamMember createTeamMember(Game game) {
        TeamMember teamMember = new TeamMember();
        teamMember.setName(NAME);
        teamMember.setGame(game);

        teamMemberRepository.save(teamMember);
        return teamMember;
    }

    @After
    public void cleanTestMethod() throws Exception {
        teamMemberRepository.deleteAll();
        gameRepository.deleteAll();
    }

    @Test
    public void getAll() throws Exception {
        createTeamMember(game);
        createTeamMember(game);
        createTeamMember(game);

        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(4))).andExpect(jsonPath("$[0].id", is(teamMember.getId())))
                .andExpect(jsonPath("$[0].name", is(teamMember.getName())));
    }

    @Test
    public void getAllByGame() throws Exception {
        Game g2 = createGame();

        createTeamMember(game);
        TeamMember tm1 = createTeamMember(g2);
        TeamMember tm2 = createTeamMember(g2);
        TeamMember tm3 = createTeamMember(g2);
        createTeamMember(game);

        String path = String.format("%s?gameId=%d", PATH, g2.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(3))).andExpect(jsonPath("$[0].id", is(tm1.getId())))
                .andExpect(jsonPath("$[1].id", is(tm2.getId()))).andExpect(jsonPath("$[2].id", is(tm3.getId())));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, teamMember.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(teamMember.getId()))).andExpect(jsonPath("name", is(teamMember.getName())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        TeamMember request = new TeamMember();
        request.setName(NAME);

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        jsonRequest.getAsJsonObject().add("game", gameObject);

        ResultActions result = perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("name", is(NAME)));
    }

    @Test
    public void createConflict() throws Exception {
        TeamMember request = new TeamMember();
        request.setId(teamMember.getId());
        request.setName(NAME);

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        jsonRequest.getAsJsonObject().add("game", gameObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isConflict());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, teamMember.getId());

        Assert.assertNotNull(teamMemberRepository.findOne(teamMember.getId()));

        perform(delete(path)).andExpect(status().isOk());

        Assert.assertNull(teamMemberRepository.findOne(teamMember.getId()));
    }

    @Test
    public void destroyNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(delete(path)).andExpect(status().isNotFound());
    }
}
