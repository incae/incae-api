
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pernix.incae.Application;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.data.TacticScoreRepository;
import com.pernix.incae.entities.Stage;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.entities.TacticScore;
import com.pernix.incae.entities.util.TacticClassification;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class TacticServicesTest extends BaseTest {

    private static final double SCORE = 3.5;
    private static final double PRICE = 12000.0;
    private static final double DAYS = 5.0;
    private static final String PATH = "/tactics";
    private static final String NAME = "Tactic name";
    private static final String DESCRIPTION = "Tactic description";
    private static final int IDEAL_POSITION = 100;
    private static final int NOT_FOUND_ID = 16357;

    @Autowired
    private TacticRepository tacticRepository;
    @Autowired
    private StageRepository stageRepository;
    @Autowired
    private TacticScoreRepository tacticScoreRepository;

    private Tactic tactic;
    private TacticScore tacticScore;
    private Stage stage;

    @Before
    public void prepareTestMethod() throws Exception {
        tacticRepository.deleteAll();
        tacticScoreRepository.deleteAll();
        stage = stageRepository.findOne(1);
        tactic = createTactic(IDEAL_POSITION);
    }

    @After
    public void cleanTestMethod() throws Exception {
        gameRepository.deleteAll();
        tacticRepository.deleteAll();
    }

    private Tactic createTactic(int idealPosition) {

        tacticScore = new TacticScore();
        tacticScore.setPerfect(SCORE);
        tacticScore.setMedium(SCORE);
        tacticScore.setLow(SCORE);
        tacticScore.setPerfect(SCORE);
        tacticScoreRepository.save(tacticScore);

        Tactic tactic = new Tactic();
        tactic.setName(NAME);
        tactic.setDescription(DESCRIPTION);
        tactic.setDays(DAYS);
        tactic.setPrice(PRICE);
        tactic.setTacticScore(tacticScore);
        tactic.setIdealPosition(idealPosition);
        tactic.setStage(stage);
        tactic.setClassification(TacticClassification.MANDATORY);

        tacticRepository.save(tactic);
        return tactic;
    }

    @Test
    public void getAll() throws Exception {
        createTactic(IDEAL_POSITION + 1);
        createTactic(IDEAL_POSITION + 2);

        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(3))).andExpect(jsonPath("$[0].id", is(tactic.getId())))
                .andExpect(jsonPath("$[0].name", is(tactic.getName())));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, tactic.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(tactic.getId()))).andExpect(jsonPath("name", is(tactic.getName())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Tactic request = new Tactic();
        request.setName(NAME);
        request.setDescription(DESCRIPTION);
        request.setDays(DAYS);
        request.setPrice(PRICE);
        request.setTacticScore(tacticScore);
        request.setIdealPosition(IDEAL_POSITION + 1);

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject stageObject = new JsonObject();
        stageObject.addProperty("id", stage.getId());
        jsonRequest.getAsJsonObject().add("stage", stageObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, tactic.getId());
        final String updatedName = "Updated name";

        Tactic request = tactic;
        request.setName(updatedName);
        request.setStage(null);

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject stageObject = new JsonObject();
        stageObject.addProperty("id", stage.getId());
        jsonRequest.getAsJsonObject().add("stage", stageObject);

        ResultActions result = perform(put(path).content(jsonRequest.toString())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("name", is(updatedName))).andExpect(jsonPath("days", is(DAYS)))
                .andExpect(jsonPath("price", is(PRICE)));
    }

    @Test
    public void updateNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);
        final String updatedName = "Updated name";

        Tactic request = tactic;
        request.setName(updatedName);
        request.setStage(null);

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isNotFound());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, tactic.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}
