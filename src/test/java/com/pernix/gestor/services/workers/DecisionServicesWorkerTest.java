package com.pernix.gestor.services.workers;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import com.pernix.incae.Application;
import com.pernix.incae.data.DepartmentInfoRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.data.TacticScoreRepository;
import com.pernix.incae.entities.Decision;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Stage;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.entities.TacticScore;
import com.pernix.incae.entities.util.GameStatus;
import com.pernix.incae.entities.util.TacticClassification;
import com.pernix.incae.services.workers.DecisionServicesWorker;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class DecisionServicesWorkerTest {

    private static final Double TEAM_SCORE = 0.0;
    private static final Double LEADERSHIP_SCORE = 0.0;
    private static final Double BUDGET = 1000000.0;
    private static final Double AVAILABLE_DAYS = 320.0;
    private static final Double TACTIC_PRICE = 12000.0;
    private static final Double TACTIC_DAYS = 5.0;
    private static final Double TACTIC_SCORE = 3.5;
    private static final int TACTIC_IDEAL_POSITION = 1;

    @Autowired
    WebApplicationContext context;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    DepartmentInfoRepository departmentInfoRepository;
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    private DecisionServicesWorker decisionWorker;
    @Autowired
    TacticRepository tacticRepository;
    @Autowired
    private StageRepository stageRepository;
    @Autowired
    TacticScoreRepository tacticScoreRepository;

    private Game game;
    private Tactic tactic;
    private Stage stage;

    @Before
    public void prepareTestMethod() throws Exception {
        tacticRepository.deleteAll();
        tacticScoreRepository.deleteAll();
        stage = stageRepository.findOne(1);
        tactic = createTactic(TACTIC_IDEAL_POSITION);
        game = createGame();
    }

    @After
    public void cleanTestMethod() throws Exception {
        gameRepository.deleteAll();
        tacticRepository.deleteAll();
    }

    private Game createGame() {
        Game game = new Game();
        game.setName("Team Name");
        game.setLeadershipChangeScore(LEADERSHIP_SCORE);
        game.setCulturalChangeScore(TEAM_SCORE);
        game.setAvailableDays(AVAILABLE_DAYS);
        game.setBudget(BUDGET);
        game.setLastVisit(new Date());
        game.setStatus(GameStatus.IN_PROGRESS);
        game.setCurrentStage(stage);
        gameRepository.save(game);
        return game;
    }

    private Tactic createTactic(int idealPosition) {

        TacticScore tacticScore = new TacticScore();
        tacticScore.setPerfect(TACTIC_SCORE);
        tacticScore.setMedium(TACTIC_SCORE);
        tacticScore.setLow(TACTIC_SCORE);
        tacticScore.setNegative(TACTIC_SCORE);

        Tactic tactic = new Tactic();
        tactic.setName("Tactic name");
        tactic.setDescription("Tactic description");
        tactic.setDays(TACTIC_DAYS);
        tactic.setPrice(TACTIC_PRICE);
        tactic.setTacticScore(tacticScore);
        tactic.setIdealPosition(idealPosition);
        tactic.setStage(stage);
        tactic.setClassification(TacticClassification.MANDATORY);

        tacticScoreRepository.save(tacticScore);
        tacticRepository.save(tactic);
        return tactic;
    }

    @Test
    public void applyDecision() {

        Decision decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic);

        decisionWorker.createDecision(decision);

        // DepartmentInfo departmentInfo = departmentInfoRepository.findOne(1);
        // departmentRepository.findByGameAndDepartmentInfo(game,
        // departmentInfo);

        // assertEquals(TACTIC_SCORE, department.getScore());

    }

    @Test
    public void precisionOnTacticPosition() {
        Tactic t2 = createTactic(TACTIC_IDEAL_POSITION + 1);
        Tactic t3 = createTactic(TACTIC_IDEAL_POSITION + 2);
        Tactic t4 = createTactic(TACTIC_IDEAL_POSITION + 3);

        assertEquals(new Double(1), decisionWorker.precisionOnDecisionPosition(tactic, 1));
        assertEquals(new Double(1), decisionWorker.precisionOnDecisionPosition(t2, 2));
        assertEquals(new Double(1), decisionWorker.precisionOnDecisionPosition(t3, 3));
        assertEquals(new Double(1), decisionWorker.precisionOnDecisionPosition(t4, 4));

        assertEquals(new Double(0.25), decisionWorker.precisionOnDecisionPosition(t4, 1));
        assertEquals(new Double(0.75), decisionWorker.precisionOnDecisionPosition(t3, 2));
        assertEquals(new Double(0.75), decisionWorker.precisionOnDecisionPosition(t2, 3));
        assertEquals(new Double(0.25), decisionWorker.precisionOnDecisionPosition(tactic, 4));
    }

    @Test
    public void updateStatusGame() {
        game.setAvailableDays(10.5);
        game.setBudget(2000.0);
        gameRepository.save(game);

        tactic.setPrice(1000.0);
        tactic.setDays(5.0);
        tacticRepository.save(tactic);

        Tactic tactic2 = createTactic(TACTIC_IDEAL_POSITION + 1);
        tactic2.setPrice(1000.0);
        tactic2.setDays(5.0);
        tacticRepository.save(tactic2);

        Tactic tactic3 = createTactic(TACTIC_IDEAL_POSITION + 2);
        tactic3.setPrice(1000.0);
        tactic3.setDays(5.0);
        tacticRepository.save(tactic3);

        Tactic tactic4 = createTactic(TACTIC_IDEAL_POSITION + 3);
        tactic4.setPrice(1000.0);
        tactic4.setDays(5.0);
        tacticRepository.save(tactic4);

        Tactic tactic5 = createTactic(TACTIC_IDEAL_POSITION + 4);
        tactic5.setPrice(1000.0);
        tactic5.setDays(5.0);
        tacticRepository.save(tactic5);

        Decision decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic);

        assertEquals(GameStatus.IN_PROGRESS, game.getStatus());

        decisionWorker.createDecision(decision);

        assertEquals(GameStatus.IN_PROGRESS, game.getStatus());
        assertEquals(new Double(5.5), game.getAvailableDays());
        assertEquals(new Double(1000.0), game.getBudget());

        decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic2);

        decisionWorker.createDecision(decision);

        assertEquals(GameStatus.FINISHED_LOST, game.getStatus());
        assertEquals(new Double(0.5), game.getAvailableDays());
        assertEquals(new Double(0.0), game.getBudget());
    }

    @Test
    public void updateStatusGame2() {
        game.setAvailableDays(9.5);
        game.setBudget(132000.0);
        gameRepository.save(game);

        tactic.setPrice(200000.0);
        tactic.setDays(20.0);
        tacticRepository.save(tactic);

        Tactic tactic2 = createTactic(TACTIC_IDEAL_POSITION + 1);
        tactic2.setPrice(200000.0);
        tactic2.setDays(20.0);
        tacticRepository.save(tactic2);

        Tactic tactic3 = createTactic(TACTIC_IDEAL_POSITION + 2);
        tactic3.setPrice(200000.0);
        tactic3.setDays(20.0);
        tacticRepository.save(tactic3);

        Tactic tactic4 = createTactic(TACTIC_IDEAL_POSITION + 3);
        tactic4.setPrice(400000.0);
        tactic4.setDays(30.0);
        tacticRepository.save(tactic4);

        Tactic tactic5 = createTactic(TACTIC_IDEAL_POSITION + 4);
        tactic5.setPrice(2500.0);
        tactic5.setDays(2.0);
        tacticRepository.save(tactic5);

        Decision decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic5);

        assertEquals(GameStatus.IN_PROGRESS, game.getStatus());

        decisionWorker.createDecision(decision);

        assertEquals(GameStatus.FINISHED_LOST, game.getStatus());
        assertEquals(new Double(7.5), game.getAvailableDays());
        assertEquals(new Double(129500.0), game.getBudget());
    }

    @Test
    public void updateStageGame() {
        gameRepository.save(game);

        tactic.setDays(20.0);
        tacticRepository.save(tactic);

        Tactic tactic2 = createTactic(TACTIC_IDEAL_POSITION + 1);
        tactic2.setDays(21.0);
        tacticRepository.save(tactic2);

        Tactic tactic3 = createTactic(TACTIC_IDEAL_POSITION + 2);
        tactic3.setDays(34.0);
        tacticRepository.save(tactic3);

        Decision decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic);

        assertEquals(0, game.getSpentDaysOnCurrentStage());
        assertEquals("Analizar", game.getCurrentStage().getName());

        decisionWorker.createDecision(decision);

        assertEquals(20, game.getSpentDaysOnCurrentStage());
        assertEquals("Analizar", game.getCurrentStage().getName());

        decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic2);
        decisionWorker.createDecision(decision);

        assertEquals(41, game.getSpentDaysOnCurrentStage());
        assertEquals("Analizar", game.getCurrentStage().getName());

        decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic3);
        decisionWorker.createDecision(decision);

        assertEquals(0, game.getSpentDaysOnCurrentStage());
        assertEquals("Mapear el talento", game.getCurrentStage().getName());

    }
}
