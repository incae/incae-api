package com.pernix.gestor.services.workers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import com.pernix.incae.Application;
import com.pernix.incae.data.DepartmentInfoRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.DepartmentInfo;
import com.pernix.incae.entities.Employee;
import com.pernix.incae.entities.Game;
import com.pernix.incae.services.workers.GameServicesWorker;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class GameServicesWorkerTest {

    private static final Double CULTURAL_CHANGE_SCORE = 0.0;
    // Average of all commitment scores
    private static final Double LEADERSHIP_SCORE = 590.0 / 22;
    private static final Double BUDGET = 1000000.0;
    private static final Double AVAILABLE_DAYS = 360.0;
    private static final String TEAM_NAME = "Team name";

    @Autowired
    WebApplicationContext context;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    DepartmentInfoRepository departmentInfoRepository;
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    private GameServicesWorker gamesWorker;

    @Before
    public void prepareTestMethod() throws Exception {
    }

    @After
    public void cleanTestMethod() throws Exception {
        gameRepository.deleteAll();
    }

    private Game newGame() {
        Game game = new Game();
        game.setName(TEAM_NAME);
        return game;
    }

    @Test
    public void createGame() {
        Game game = newGame();
        gamesWorker.createGame(game);

        assertTrue(game.getId() > 0);

        int id = game.getId();

        Game createdGame = gameRepository.findOne(id);

        assertNotNull(createdGame);
        assertEquals(TEAM_NAME, createdGame.getName());
        assertEquals(CULTURAL_CHANGE_SCORE, createdGame.getCulturalChangeScore());
        assertEquals(AVAILABLE_DAYS, createdGame.getAvailableDays());
        assertEquals(BUDGET, createdGame.getBudget());
        assertEquals(LEADERSHIP_SCORE, createdGame.getLeadershipChangeScore());
        assertEquals("Analizar", createdGame.getCurrentStage().getName());

        Set<Department> departments = departmentRepository.findAllByGame(createdGame);
        assertNotNull(departments);
        assertEquals(8, departments.size());

        DepartmentInfo departmentInfo = departmentInfoRepository.findOne(1);

        Department department = departmentRepository.findByGameAndDepartmentInfo(game, departmentInfo);
        assertNotNull(department);

        Set<Employee> employees = employeeRepository.findAllByDepartment(department);
        assertNotNull(employees);
        assertEquals(1, employees.size());
    }

    @Test
    public void updateLastVisit() {
        Game game = newGame();
        gamesWorker.createGame(game);

        assertTrue(game.getId() > 0);

        Date lastVisit = game.getLastVisit();

        gamesWorker.updateLastVisit(game);

        Game updatedGame = gameRepository.findOne(game.getId());

        assertTrue(updatedGame.getLastVisit().getTime() > lastVisit.getTime());
    }
}
