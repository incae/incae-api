package com.pernix.gestor.services;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.Date;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import com.pernix.incae.AppConfig;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.RoleRepository;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.data.UserRepository;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Role;
import com.pernix.incae.entities.Stage;
import com.pernix.incae.entities.Users;
import com.pernix.incae.entities.util.GameStatus;

public class BaseTest {
    private static final String OAUTH_TOKEN_URL = "/oauth/token";
    private static final String GRANT_TYPE = "grant_type";
    private static final String AUTHORIZED_USERNAME = "testUser";
    private static final String AUTHORIZED_PASSWORD = "testUserPassword";
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";
    protected static final String TEAM_NAME = "Team name";
    protected static final double CULTURALCHANGE_SCORE = 0.0;
    // Average of all commitment scores
    protected static final Double LEADERSHIP_SCORE = 590.0 / 22;
    protected static final double BUDGET = 1000000.0;
    protected static final double AVAILABLE_DAYS = 360.0;

    @Autowired
    WebApplicationContext context;
    @Autowired
    protected GameRepository gameRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private StageRepository stageRepository;
    @Autowired
    protected AppConfig appConfig;

    protected Users user;
    private String userAccessToken;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;
    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();

        if (user == null) {
            user = userRepository.findByLogin(AUTHORIZED_USERNAME);
        }
        if (user == null) {
            Role role = roleRepository.findOne(1);
            user = new Users();
            user.setLogin(AUTHORIZED_USERNAME);
            user.setName("Test User");
            user.setPassword(AUTHORIZED_PASSWORD);
            user.setRole(role);
            userRepository.save(user);
        }

        if (userAccessToken == null) {
            userAccessToken = getAccessToken(AUTHORIZED_USERNAME, AUTHORIZED_PASSWORD);
        }
    }

    private String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic " + new String(Base64Utils
                .encode((appConfig.getIncaeOauthClientId() + ":" + appConfig.getIncaeOauthSecret()).getBytes()));
        String content = mvc
                .perform(post(OAUTH_TOKEN_URL).header("Authorization", authorization)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED).param(USERNAME, username)
                        .param(PASSWORD, password).param(GRANT_TYPE, PASSWORD))
                .andReturn().getResponse().getContentAsString();
        return content.substring(17, 53);
    }

    protected ResultActions perform(MockHttpServletRequestBuilder request) throws Exception {
        return perform(request.contentType(MediaType.APPLICATION_JSON), false);
    }

    protected ResultActions performAuthorized(MockHttpServletRequestBuilder request) throws Exception {
        return perform(request.contentType(MediaType.APPLICATION_JSON), true);
    }

    private ResultActions perform(MockHttpServletRequestBuilder request, boolean performAuthorized) throws Exception {
        if (performAuthorized) {
            request = request.header("Authorization", "Bearer " + userAccessToken);
        }
        return mvc.perform(request.contentType(MediaType.APPLICATION_JSON));
    }

    protected Game createGame() {
        Game game = new Game();
        game.setName(TEAM_NAME);
        game.setCulturalChangeScore(CULTURALCHANGE_SCORE);
        game.setLeadershipChangeScore(LEADERSHIP_SCORE);
        game.setAvailableDays(AVAILABLE_DAYS);
        game.setBudget(BUDGET);
        game.setLastVisit(new Date());
        game.setStatus(GameStatus.IN_PROGRESS);
        Stage stage = stageRepository.findOne(1);
        game.setCurrentStage(stage);
        gameRepository.save(game);
        return game;
    }
}
