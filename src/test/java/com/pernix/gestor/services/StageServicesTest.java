
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.pernix.incae.Application;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.entities.Stage;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class StageServicesTest extends BaseTest {

    private static final String PATH = "/stages";
    private static final int NOT_FOUND_ID = 16357;

    @Autowired
    private StageRepository stageRepository;

    private Stage stage;

    @Before
    public void prepareTestMethod() throws Exception {
        stage = stageRepository.findOne(1);
    }

    @After
    public void cleanTestMethod() throws Exception {
    }

    @Test
    public void getAll() throws Exception {
        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(5))).andExpect(jsonPath("$[4].id", is(stage.getId())))
                .andExpect(jsonPath("$[4].name", is(stage.getName())));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, stage.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(stage.getId()))).andExpect(jsonPath("name", is(stage.getName())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Stage request = new Stage();

        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, stage.getId());
        final String updatedName = "Updated name";
        final int maximumDays = 30;
        final int minimumDays = 30;

        Stage request = stage;
        request.setName(updatedName);
        request.setMaximumDays(maximumDays);
        request.setMinimumDays(minimumDays);
        request.setNextStage(null);
        request.setTactics(null);

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, stage.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}
