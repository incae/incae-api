
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.pernix.incae.Application;
import com.pernix.incae.data.DepartmentInfoRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.DepartmentInfo;
import com.pernix.incae.entities.Game;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class DepartmentServicesTest extends BaseTest {

    private static final String PATH = "/departments";
    private static final Double DEPARTMENT_LEADERSHIP_CHANGE_SCORE = 14.3;
    private static final int NOT_FOUND_ID = 16357;

    @Autowired
    private DepartmentInfoRepository departmentInfoRepository;
    @Autowired
    private DepartmentRepository departmentRepository;

    private Game game;
    private DepartmentInfo departmentInfo;
    private Department department;

    @Before
    public void prepareTestMethod() throws Exception {
        departmentInfo = departmentInfoRepository.findOne(1);
        game = createGame();
        department = createDepartment();
    }

    @After
    public void cleanTestMethod() throws Exception {
        departmentRepository.deleteAll();
        gameRepository.deleteAll();
    }

    private Department createDepartment() {
        Department department = new Department();
        department.setDepartmentInfo(departmentInfo);
        department.setGame(game);
        department.setLeadershipChangeScore(DEPARTMENT_LEADERSHIP_CHANGE_SCORE);
        departmentRepository.save(department);
        return department;
    }

    @Test
    public void getAllByGame() throws Exception {
        createDepartment();
        createDepartment();
        createDepartment();

        String path = String.format("%s?gameId=%d", PATH, game.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].leadershipChangeScore", is(DEPARTMENT_LEADERSHIP_CHANGE_SCORE)))
                .andExpect(jsonPath("$[0].departmentInfo.id", is(departmentInfo.getId())))
                .andExpect(jsonPath("$[0].departmentInfo.name", is(departmentInfo.getName())));
    }

    @Test
    public void getAllByGameNotFound() throws Exception {
        String path = String.format("%s?gameId=%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, department.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(department.getId())))
                .andExpect(jsonPath("leadershipChangeScore", is(DEPARTMENT_LEADERSHIP_CHANGE_SCORE)))
                .andExpect(jsonPath("departmentInfo.id", is(departmentInfo.getId())))
                .andExpect(jsonPath("departmentInfo.name", is(departmentInfo.getName())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Department request = new Department();

        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, department.getId());
        Department request = department;
        request.setLeadershipChangeScore(23.32);
        request.setGame(null);
        request.setDepartmentInfo(null);
        request.setEmployees(null);

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, department.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}
