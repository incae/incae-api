
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.pernix.incae.Application;
import com.pernix.incae.entities.Game;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class GameServicesTest extends BaseTest {

    private static final String PATH = "/games";
    private static final int NOT_FOUND_ID = 16357;

    private Game game;

    @Before
    public void prepareTestMethod() throws Exception {
        game = createGame();
    }

    @After
    public void cleanTestMethod() throws Exception {
        gameRepository.deleteAll();
    }

    @Test
    public void getAll() throws Exception {
        createGame();
        createGame();

        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(3))).andExpect(jsonPath("$[0].id", is(game.getId())))
        .andExpect(jsonPath("$[0].name", is(game.getName())))
        .andExpect(jsonPath("$[0].lastVisit", is(game.getLastVisit().getTime())));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, game.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(game.getId()))).andExpect(jsonPath("name", is(game.getName())))
        .andExpect(jsonPath("lastVisit", greaterThan(game.getLastVisit().getTime())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Game request = new Game();
        request.setName("test1");

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        jsonRequest.getAsJsonObject().addProperty("lastVisit", (new Date()).getTime());

        ResultActions result = perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("name", is("test1")))
        .andExpect(jsonPath("culturalChangeScore", is(CULTURALCHANGE_SCORE)))
        .andExpect(jsonPath("availableDays", is(AVAILABLE_DAYS))).andExpect(jsonPath("budget", is(BUDGET)))
        .andExpect(jsonPath("leadershipChangeScore", is(LEADERSHIP_SCORE)));
    }

    @Test
    public void createConflict() throws Exception {
        Game request = new Game();
        request.setId(game.getId());
        request.setName(TEAM_NAME);

        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isConflict());
    }

    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, game.getId());
        final String updatedName = "Updated name";

        Game request = game;
        request.setName(updatedName);
        request.setDecisions(null);
        request.setDepartments(null);
        request.setCurrentStage(null);

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void destroyUnauthorized() throws Exception {
        String path = String.format("%s/%d", PATH, game.getId());

        Assert.assertNotNull(gameRepository.findOne(game.getId()));

        perform(delete(path)).andExpect(status().isUnauthorized());

        Assert.assertNotNull(gameRepository.findOne(game.getId()));
    }

    @Test
    public void destroyAuthorized() throws Exception {
        String path = String.format("%s/%d", PATH, game.getId());

        Assert.assertNotNull(gameRepository.findOne(game.getId()));

        performAuthorized(delete(path)).andExpect(status().isOk());

        Assert.assertNull(gameRepository.findOne(game.getId()));
    }

    @Test
    public void destroyNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        performAuthorized(delete(path)).andExpect(status().isNotFound());
    }
}
