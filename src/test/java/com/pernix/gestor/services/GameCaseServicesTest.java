package com.pernix.gestor.services;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.pernix.incae.Application;
import com.pernix.incae.data.GameCaseInfoRepository;
import com.pernix.incae.data.GameCaseRepository;
import com.pernix.incae.entities.GameCase;
import com.pernix.incae.entities.GameCaseInfo;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class GameCaseServicesTest extends BaseTest {

    private static final String PATH = "/cases";

    private GameCase gameCase;

    @Autowired
    private GameCaseRepository gameCaseRepository;

    @Autowired
    private GameCaseInfoRepository gameCaseInfoRepository;

    @Before
    public void prepareTestMethod() throws Exception {
        gameCase = createGameCase();
    }

    @After
    public void cleanTestMethod() throws Exception {
        gameCaseRepository.deleteAll();
    }

    @Test
    public void getAll() throws Exception {
        ResultActions result = perform(get(PATH))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[1].id", is(gameCase.getId())))
            .andExpect(jsonPath("$[1].name", is(gameCase.getName())));
    }

    private GameCase createGameCase() {
        gameCase = new GameCase();
        Set<GameCaseInfo> gameCaseInfoSet = createGameCaseInfo();

        gameCase.setName("CASA AUGUSTA");
        gameCase.setGameCaseInfoSet(gameCaseInfoSet);

        gameCaseInfoRepository.save(gameCaseInfoSet);
        gameCaseRepository.save(gameCase);

        return gameCase;
    }

    private Set<GameCaseInfo> createGameCaseInfo() {
        GameCaseInfo gameCaseInfo1 = new GameCaseInfo();
        gameCaseInfo1.setTitle("CAMBIOS EN LA INDUSTRIA DEL RETAIL");

        gameCaseInfo1.setContent("Todos los negocios dedicados a la venta de bienes y servicios a consumidores finales" +
            " son parte de la industria de ventas al detalle o retail. La clasificación más generalizada de las" +
            " empresas en esta industria las separa en tres tipos: tiendas por departamentos, tiendas especializadas" +
            " y tiendas de descuentos.</p> <p>La industria del retail ha evolucionado significativamente pasando de" +
            " procesos made-to-order (hecho contra demanda) a venta de productos ready-to-use (listo para usar). Los" +
            " mayores impulsores de cambios en este rubro han sido los avances tecnológicos que transforman los" +
            " paradigmas de producción y los cambios generacionales que afectan las preferencias y el poder" +
            " adquisitivo de los consumidores. Un ejemplo clásico de un cambio substancial fue la revolución" +
            " en la industria de la moda en la década de 1950. Los talleres de Alta Costura, que confeccionaban" +
            " ropa hecha a la medida y con diseños exclusivos,  fueron desplazados por el prêt-à-porter. Esta" +
            " tendencia tenía como objetivo llegar a un público más amplio, permitió industrializar la producción" +
            " y hacer un uso más eficiente de los recursos económicos.</p> <p>El inicio de la era digital ha" +
            " provocado cambios en los modelos de negocios de las empresas minoristas. Ha presentado oportunidades" +
            " para incrementar el alcance en el mercado llegando a más personas a través de múltiples canales." +
            " Así mismo, el incremento de la bancarización ha permitido la diversificación de los métodos de" +
            " pago.</p>");
        gameCaseInfo1.setGameCase(gameCase);

        GameCaseInfo gameCaseInfo2 = new GameCaseInfo();
        gameCaseInfo2.setTitle("EL FUTURO DE LA INDUSTRIA RETAIL");

        gameCaseInfo2.setContent("<p>El Foro Económico Mundial ha indicado que la principal característica que " +
            "marcará a la industria de ventas al detalle durante la próxima década es el mayor control que los " +
            "compradores tendrán sobre los productos y servicios que consumirán, a la vez que dispondrán de una " +
            "mayor variedad de opciones de compra y metodos de pago. Las compras al detalle se están convirtiendo " +
            "así, a ritmo acelerado, en experiencias personalizadas, simples y convenientes. </p><p>En el reporte " +
            "de “El Futuro del Retail” del Foro Económico Mundial  se mencionan 3 tendencias importantes a " +
            "considerar para analizar el modelo de negocios de las empresas minoristas: </p> <p>1.  Clientes " +
            "empoderados: Los clientes tomarán las decisiones de compra basados en los beneficios tangibles e " +
            "intangibles que perciben al adquirir productos y servicios. Tradicionalmente, estos beneficios se " +
            "evalúan utilizando tres dimensiones de referencia: costo, opción y conveniencia. Sin embargo, estas " +
            "dimensiones han evolucionado y se han agregado dos más: control y experiencia.   Esto se traduce en " +
            "una expectativa de mayor influencia. </p> <p>2.  Tecnologías disruptivas:  Se destacan 8 tecnologías " +
            "disruptivas que modificaran la dinámica de la industria del reatail en los próximos 10 años. Estas " +
            "son: </p> <p>a.  Internet of Things (IoT) </p> <p>b.  Vehículos autónomos (AV / drones) </p> <p>c." +
            "  Inteligencia Artificial (AI) / aprendizaje de la máquina </p> <p>d.  Robótica </p> <p>e.  " +
            "Trazabilidad digital </p> <p>f.  Impresión 3D </p> <p>g.  Realidad aumentada (AR) / realidad virtual " +
            "(VR) </p> <p>h.  Blockchain </p> <p>Para poder mantenerse relevantes en la nueva economía, las " +
            "empresas de retail deben poder analizar 3 aspectos fundamentales de cada una de estas tecnologías: " +
            "los beneficios del negocio, la preparación y el efecto transformacional.</p> <p>3. Modelos de " +
            "negocios transformadores: actualmente ya se pueden observar modelos de negocios de retail que " +
            "mezclan modelos online y offline. La expectativa es que la industria sufra una transición hacia un " +
            "modelo que permita la expansión en el canal digital. Las tiendas físicas deberán evolucionar de ser " +
            "un canal de distribución (modelo actual) a serlugares que vendan historias y sean plataformas para " +
            "generar compromiso y lealtad, experiencias diferenciadas e interacción con la marca.</p>");
        gameCaseInfo2.setGameCase(gameCase);

        Set<GameCaseInfo> gameCaseInfoSet = new HashSet<>();
        gameCaseInfoSet.add(gameCaseInfo1);
        gameCaseInfoSet.add(gameCaseInfo2);

        return gameCaseInfoSet;
    }
}
