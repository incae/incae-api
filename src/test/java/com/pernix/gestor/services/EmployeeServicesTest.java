
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.pernix.incae.Application;
import com.pernix.incae.data.DepartmentInfoRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeInfoRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.DepartmentInfo;
import com.pernix.incae.entities.Employee;
import com.pernix.incae.entities.EmployeeInfo;
import com.pernix.incae.entities.Game;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class EmployeeServicesTest extends BaseTest {

    private static final String PATH = "/employees";
    private static final Double INITIAL_EMPLOYEE_COMMITMENT_LEVEL = 4.0;
    private static final int NOT_FOUND_ID = 16357;

    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private DepartmentInfoRepository departmentInfoRepository;
    @Autowired
    private EmployeeInfoRepository employeeInfoRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    private DepartmentInfo departmentInfo;
    private EmployeeInfo employeeInfo;
    private Game game;
    private Employee employee;
    private Department department;

    @Before
    public void prepareTestMethod() throws Exception {
        departmentInfo = departmentInfoRepository.findOne(1);
        employeeInfo = employeeInfoRepository.findOne(1);
        game = createGame();
        department = createDepartment();
        employee = createEmployee();
    }

    @After
    public void cleanTestMethod() throws Exception {
        employeeRepository.deleteAll();
        departmentRepository.deleteAll();
        gameRepository.deleteAll();
    }

    private Department createDepartment() {
        Department department = new Department();
        department.setDepartmentInfo(departmentInfo);
        department.setGame(game);
        department.setLeadershipChangeScore(0.00);
        departmentRepository.save(department);
        return department;
    }

    private Employee createEmployee() {
        Employee employee = new Employee();
        employee.setEmployeeInfo(employeeInfo);
        employee.setDepartment(department);
        employee.setCommitmentLevel(INITIAL_EMPLOYEE_COMMITMENT_LEVEL);
        employeeRepository.save(employee);
        return employee;
    }

    @Test
    public void getAllByGameDepartment() throws Exception {
        createEmployee();
        createEmployee();
        createEmployee();
        createEmployee();

        String path = String.format("%s?departmentId=%d", PATH, department.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].commitmentLevel", is(INITIAL_EMPLOYEE_COMMITMENT_LEVEL)))
                .andExpect(jsonPath("$[0].employeeInfo.id", is(employeeInfo.getId())))
                .andExpect(jsonPath("$[0].employeeInfo.name", is(employeeInfo.getName())));
    }

    @Test
    public void getAllByGameDepartmentNotFound() throws Exception {
        String path = String.format("%s?departmentId=%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, employee.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(employee.getId())))
                .andExpect(jsonPath("commitmentLevel", is(INITIAL_EMPLOYEE_COMMITMENT_LEVEL)))
                .andExpect(jsonPath("employeeInfo.id", is(employeeInfo.getId())))
                .andExpect(jsonPath("employeeInfo.name", is(employeeInfo.getName())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Employee request = new Employee();

        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, employee.getId());
        Employee request = employee;
        request.setCommitmentLevel(23.32);
        request.setEmployeeInfo(null);
        request.setDepartment(null);

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, employee.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}