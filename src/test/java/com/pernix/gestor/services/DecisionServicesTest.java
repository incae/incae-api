
package com.pernix.gestor.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pernix.incae.Application;
import com.pernix.incae.data.DecisionRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.data.TacticScoreRepository;
import com.pernix.incae.entities.Decision;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Stage;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.entities.TacticScore;
import com.pernix.incae.entities.util.TacticClassification;
import com.pernix.incae.services.workers.GameServicesWorker;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class DecisionServicesTest extends BaseTest {

    private static final double SCORE = 10.0;
    private static final double PRICE = 12000.0;
    private static final double DAYS = 5.0;
    private static final String PATH = "/decisions";
    private static final String NAME = "Tactic name";
    private static final String DESCRIPTION = "Tactic description";
    private static final int IDEAL_POSITION = 100;
    private static final int NOT_FOUND_ID = 16357;

    @Autowired
    private TacticRepository tacticRepository;
    @Autowired
    private StageRepository stageRepository;
    @Autowired
    private DecisionRepository decisionRepository;
    @Autowired
    private TacticScoreRepository tacticScoreRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private GameServicesWorker gameWorker;

    private Game game;
    private Tactic tactic;
    private Stage stage;
    private TacticScore tacticScore;

    @Before
    public void prepareTestMethod() throws Exception {
        tacticRepository.deleteAll();
        tacticScoreRepository.deleteAll();
        game = createGame();
        gameWorker.createGame(game);
        stage = stageRepository.findOne(1);
        tactic = createTactic(IDEAL_POSITION);

    }

    @After
    public void cleanTestMethod() throws Exception {
        gameRepository.deleteAll();
        tacticRepository.deleteAll();
        departmentRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    private Tactic createTactic(int idealPosition) {

        tacticScore = new TacticScore();
        tacticScore.setPerfect(SCORE);
        tacticScore.setMedium(SCORE);
        tacticScore.setLow(SCORE);
        tacticScore.setNegative(SCORE);
        tacticScoreRepository.save(tacticScore);

        Tactic tactic = new Tactic();
        tactic.setName(NAME);
        tactic.setDescription(DESCRIPTION);
        tactic.setDays(DAYS);
        tactic.setPrice(PRICE);
        tactic.setTacticScore(tacticScore);
        tactic.setIdealPosition(idealPosition);
        tactic.setStage(stage);
        tactic.setClassification(TacticClassification.MANDATORY);

        tacticRepository.save(tactic);
        return tactic;
    }

    private Decision createDecision(Tactic tactic) {
        Decision decision = new Decision();
        decision.setGame(game);
        decision.setTactic(tactic);
        decision.setPosition(1);
        decision.setGainScore(1.0);
        decisionRepository.save(decision);
        return decision;
    }

    @Test
    public void getAllByGame() throws Exception {
        createTactic(IDEAL_POSITION + 1);
        Tactic tactic3 = createTactic(IDEAL_POSITION + 2);
        Tactic tactic4 = createTactic(IDEAL_POSITION + 3);
        createTactic(IDEAL_POSITION + 4);

        Decision decision = createDecision(tactic);
        Decision decision2 = createDecision(tactic3);
        Decision decision3 = createDecision(tactic4);

        Decision dForAnotherGame = new Decision();
        dForAnotherGame.setGame(createGame());
        dForAnotherGame.setTactic(tactic);
        dForAnotherGame.setPosition(1);
        dForAnotherGame.setGainScore(1.0);
        decisionRepository.save(dForAnotherGame);

        String path = String.format("%s?gameId=%d", PATH, game.getId());

        ResultActions result = perform(get(path))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id", is(decision.getId())))
                .andExpect(jsonPath("$[0].game").doesNotExist())
                .andExpect(jsonPath("$[1].id", is(decision2.getId())))
                .andExpect(jsonPath("$[1].game").doesNotExist())
                .andExpect(jsonPath("$[2].id", is(decision3.getId())))
                .andExpect(jsonPath("$[2].game").doesNotExist());
    }

    @Test
    public void getById() throws Exception {
        Decision decision = createDecision(tactic);
        String path = String.format("%s/%d", PATH, decision.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(decision.getId())))
        .andExpect(jsonPath("gainScore", is(decision.getGainScore())))
        .andExpect(jsonPath("position", is(decision.getPosition())))
        .andExpect(jsonPath("tactic.id", is(tactic.getId())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", tactic.getId());

        JsonObject employeeObject = new JsonObject();
        employeeObject.addProperty("id", 2);
        JsonArray employeesObject = new JsonArray();
        employeesObject.add(employeeObject);

        jsonRequest.getAsJsonObject().add("game", gameObject);
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);
        jsonRequest.getAsJsonObject().add("selectedEmployees", employeesObject);


        ResultActions result = perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("tactic.id", is(tactic.getId())))
        .andExpect(jsonPath("gainScore", is(tactic.getTacticScore().getPerfect())))
        .andExpect(jsonPath("position", is(1)));
    }

    @Test
    public void createNoGameBadRequest() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", tactic.getId());
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isBadRequest());
    }

    @Test
    public void createNoTacticBadRequest() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        jsonRequest.getAsJsonObject().add("game", gameObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isBadRequest());
    }

    @Test
    public void createGameNotFound() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", NOT_FOUND_ID);
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", tactic.getId());

        jsonRequest.getAsJsonObject().add("game", gameObject);
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isNotFound());
    }

    @Test
    public void createTacticNotFound() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", NOT_FOUND_ID);

        jsonRequest.getAsJsonObject().add("game", gameObject);
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isNotFound());
    }

    @Test
    public void createNotBudget() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", tactic.getId());

        jsonRequest.getAsJsonObject().add("game", gameObject);
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);

        tactic.setPrice(35.35);
        tacticRepository.save(tactic);
        game.setBudget(35.34);
        gameRepository.save(game);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isPreconditionFailed());
    }

    @Test
    public void createNotAvailableDays() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", tactic.getId());

        jsonRequest.getAsJsonObject().add("game", gameObject);
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);

        tactic.setDays(5.0);
        tacticRepository.save(tactic);
        game.setAvailableDays(4.9);
        gameRepository.save(game);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isPreconditionFailed());
    }

    @Test
    public void createTwice() throws Exception {
        Decision request = new Decision();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("id", game.getId());
        JsonObject tacticObject = new JsonObject();
        tacticObject.addProperty("id", tactic.getId());

        jsonRequest.getAsJsonObject().add("game", gameObject);
        jsonRequest.getAsJsonObject().add("tactic", tacticObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isConflict());
    }

    @Test
    public void update() throws Exception {
        Decision decision = createDecision(tactic);
        String path = String.format("%s/%d", PATH, decision.getId());

        Decision request = new Decision();

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void destroy() throws Exception {
        Decision decision = createDecision(tactic);
        String path = String.format("%s/%d", PATH, decision.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}
