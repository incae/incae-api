package com.pernix.incae.data;

import com.pernix.incae.entities.Board;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;


public interface BoardRepository extends CrudRepository<Board, Integer> {

    Board findByGameAndTactic(Game game, Tactic tactic);

    Set<Board> findAllByGameOrderByPosYAsc(Game game);

    Set<Board> findAllByGame(Game game);
}
