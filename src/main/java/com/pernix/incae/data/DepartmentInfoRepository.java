package com.pernix.incae.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.DepartmentInfo;

public interface DepartmentInfoRepository extends CrudRepository<DepartmentInfo, Integer> {
}
