package com.pernix.incae.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    Set<Employee> findAllByDepartment(Department department);
}
