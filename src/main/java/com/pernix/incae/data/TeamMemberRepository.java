package com.pernix.incae.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.TeamMember;
import com.pernix.incae.entities.Users;

public interface TeamMemberRepository extends CrudRepository<TeamMember, Integer> {

    Set<TeamMember> findByGameId(Integer id);
}
