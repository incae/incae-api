package com.pernix.incae.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Users;

public interface UserRepository extends CrudRepository<Users, Integer> {

    Users findByLogin(String login);

    Iterable<Users> findByRoleName(String roleName);
}
