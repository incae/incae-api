package com.pernix.incae.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Stage;

public interface StageRepository extends CrudRepository<Stage, Integer> {
}
