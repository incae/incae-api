package com.pernix.incae.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findById(int id);
}
