package com.pernix.incae.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Tactic;

public interface TacticRepository extends CrudRepository<Tactic, Integer> {

  Set<Tactic> findAll();
  
  Set<Tactic> findByIdealPosition(int idealPosition);

}