package com.pernix.incae.data;

import com.pernix.incae.entities.GameCase;

import org.springframework.data.repository.CrudRepository;

public interface GameCaseRepository extends CrudRepository<GameCase, Integer> {
}