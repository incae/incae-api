package com.pernix.incae.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.EmployeeInfo;

public interface EmployeeInfoRepository extends CrudRepository<EmployeeInfo, Integer> {

    Set<EmployeeInfo> findAllByDepartmentInfoId(Integer id);

}
