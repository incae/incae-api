package com.pernix.incae.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.TacticScore;

public interface TacticScoreRepository extends CrudRepository<TacticScore, Integer> {
}
