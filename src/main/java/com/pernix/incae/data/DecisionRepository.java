package com.pernix.incae.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Decision;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.entities.util.TacticClassification;

public interface DecisionRepository extends CrudRepository<Decision, Integer> {

    Decision findByGameAndTactic(Game game, Tactic tactic);

    Decision findFirstByGameOrderByPositionDesc(Game game);

    Set<Decision> findByGameAndClassification(Game game, TacticClassification classification);

    Set<Decision> findAllByGame(Game game);
}