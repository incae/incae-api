package com.pernix.incae.data;

import com.pernix.incae.entities.GameCaseInfo;

import org.springframework.data.repository.CrudRepository;

public interface GameCaseInfoRepository extends CrudRepository<GameCaseInfo, Integer> {
}