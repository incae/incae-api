package com.pernix.incae.data;

import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Users;
import java.util.Set;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Integer> {

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM Game u WHERE u.name = ?1")
    public boolean existsByName(String name);

    Game findByUser(Users user);

    Set<Game> findAllByOrderByCulturalChangeScoreDescBudgetDescAvailableDaysDesc();
}
