package com.pernix.incae.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.DepartmentInfo;
import com.pernix.incae.entities.Game;

public interface DepartmentRepository extends CrudRepository<Department, Integer> {

    Set<Department> findAllByGame(Game game);

    Department findByGameAndDepartmentInfo(Game game, DepartmentInfo departmentInfo);
}
