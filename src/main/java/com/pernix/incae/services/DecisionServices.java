package com.pernix.incae.services;

import com.pernix.incae.data.DecisionRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.entities.Decision;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.services.pojos.StageResultResponse;
import com.pernix.incae.services.workers.DecisionServicesWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class DecisionServices {

    private final GameRepository gameRepository;
    private final TacticRepository tacticRepository;
    private final DecisionRepository decisionRepository;
    private final DecisionServicesWorker decisionServicesWorker;

    @Autowired
    public DecisionServices(GameRepository gameRepository, TacticRepository tacticRepository,
            DecisionRepository decisionRepository, DecisionServicesWorker decisionServicesWorker) {
        this.gameRepository = gameRepository;
        this.tacticRepository = tacticRepository;
        this.decisionRepository = decisionRepository;
        this.decisionServicesWorker = decisionServicesWorker;
    }

    @RequestMapping(value = "/decisions", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Decision>> getAll(@RequestParam("gameId") int gameId) {
        Game game = gameRepository.findOne(gameId);
        if (game == null) {
            return new ResponseEntity<Iterable<Decision>>(HttpStatus.NOT_FOUND);
        }
        Iterable<Decision> decisions = decisionRepository.findAllByGame(game);
        if (decisions == null) {
            return new ResponseEntity<Iterable<Decision>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Decision>>(decisions, HttpStatus.OK);
    }

    @RequestMapping(value = "/decisions-by-stage", method = RequestMethod.GET)
    public ResponseEntity<Iterable<StageResultResponse>> getAllDecisionsByStage(@RequestParam("gameId") int gameId) {
        Game game = gameRepository.findOne(gameId);
        if (game == null) {
            return new ResponseEntity<Iterable<StageResultResponse>>(HttpStatus.NOT_FOUND);
        }
        Iterable<StageResultResponse> results = decisionServicesWorker.getDecisionsByStage(game);
        return new ResponseEntity<Iterable<StageResultResponse>>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/decisions/{id}", method = RequestMethod.GET)
    public ResponseEntity<Decision> get(@PathVariable int id) {
        Decision decision = decisionRepository.findOne(id);
        if (decision == null) {
            return new ResponseEntity<Decision>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Decision>(decision, HttpStatus.OK);
    }

    @RequestMapping(value = "/decisions", method = RequestMethod.POST)
    public ResponseEntity<Decision> create(@RequestBody Decision decision) {
        if (decision.getGame() == null || decision.getTactic() == null) {
            return new ResponseEntity<Decision>(HttpStatus.BAD_REQUEST);
        }

        Game game = gameRepository.findOne(decision.getGame().getId());
        Tactic tactic = tacticRepository.findOne(decision.getTactic().getId());
        if (game == null || tactic == null) {
            return new ResponseEntity<Decision>(HttpStatus.NOT_FOUND);
        }

        decision.setGame(game);
        decision.setTactic(tactic);

        Decision existingDecision = decisionRepository.findByGameAndTactic(game, tactic);
        if (existingDecision != null) {
            return new ResponseEntity<Decision>(existingDecision, HttpStatus.OK);
        }

        if (game.getBudget() < tactic.getPrice() || game.getAvailableDays() < tactic.getDays()) {
            return new ResponseEntity<Decision>(HttpStatus.PRECONDITION_FAILED);
        }

        decisionServicesWorker.createDecision(decision);
        Decision result = decisionRepository.findByGameAndTactic(game, tactic);
        return new ResponseEntity<Decision>(result, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/all-decisions-by-stage", method = RequestMethod.GET)
    public ResponseEntity<Iterable<StageResultResponse>> getAllDecisions() {
        Iterable<Game> games = gameRepository.findAll();
        if (games == null) {
            return new ResponseEntity<Iterable<StageResultResponse>>(HttpStatus.NO_CONTENT);
        }
        Iterable<StageResultResponse> results = decisionServicesWorker.getAllDecisionsByStage();
        return new ResponseEntity<Iterable<StageResultResponse>>(results, HttpStatus.OK);
    }
}
