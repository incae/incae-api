package com.pernix.incae.services;

import com.pernix.incae.data.GameCaseRepository;
import com.pernix.incae.entities.GameCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameCaseServices {

    private final GameCaseRepository gameCaseRepository;

    @Autowired
    public GameCaseServices(GameCaseRepository gameCaseRepository) {
        this.gameCaseRepository = gameCaseRepository;
    }

    @RequestMapping(value = "/cases", method = RequestMethod.GET)
    public ResponseEntity<Iterable<GameCase>> getAll() {
        Iterable<GameCase> cases = gameCaseRepository.findAll();
        if (cases == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(cases, HttpStatus.OK);
    }
}
