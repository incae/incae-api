package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.entities.Tactic;

@RestController
public class TacticServices {

    private final TacticRepository tacticRepository;

    @Autowired
    public TacticServices(TacticRepository tacticRepository) {
        this.tacticRepository = tacticRepository;
    }

    @RequestMapping(value = "/tactics", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Tactic>> getAll() {
        Iterable<Tactic> tactics = tacticRepository.findAll();

        if (tactics == null) {
            return new ResponseEntity<Iterable<Tactic>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Iterable<Tactic>>(tactics, HttpStatus.OK);
    }

    @RequestMapping(value = "/tactics/{id}", method = RequestMethod.GET)
    public ResponseEntity<Tactic> get(@PathVariable int id) {
        Tactic tactic = tacticRepository.findOne(id);
        if (tactic == null) {
            return new ResponseEntity<Tactic>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Tactic>(tactic, HttpStatus.OK);
    }

    @RequestMapping(value = "/tactics/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Tactic> update(@PathVariable("id") int id, @RequestBody Tactic tactic) {
        Tactic current = tacticRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<Tactic>(HttpStatus.NOT_FOUND);
        }
        tactic.setId(id);
        tacticRepository.save(tactic);
        return new ResponseEntity<Tactic>(current, HttpStatus.OK);
    }
}
