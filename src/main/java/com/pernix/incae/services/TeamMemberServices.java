package com.pernix.incae.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.TeamMemberRepository;
import com.pernix.incae.data.UserRepository;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.TeamMember;
import com.pernix.incae.services.workers.TeamMemberServicesWorker;

@RestController
public class TeamMemberServices {

    private final TeamMemberRepository teamMemberRepository;
    private final TeamMemberServicesWorker teamMemberServicesWorker;
    private final GameRepository gameRepository;

    @Autowired
    public TeamMemberServices(TeamMemberRepository teamMemberRepository, UserRepository userRepository, TeamMemberServicesWorker teamMemberServicesWorker, GameRepository gameRepository) {
        this.teamMemberRepository = teamMemberRepository;
        this.teamMemberServicesWorker = teamMemberServicesWorker;
        this.gameRepository = gameRepository;
    }

    @RequestMapping(value = "/teamMembers", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeamMember>> getAll(@RequestParam("gameId") Optional<Integer> gameId) {
        Iterable<TeamMember> teamMembers;

        if (gameId.isPresent()) {
            teamMembers = teamMemberRepository.findByGameId(gameId.get());
        } else {
            teamMembers = teamMemberRepository.findAll();
        }

        if (teamMembers == null) {
            return new ResponseEntity<Iterable<TeamMember>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<TeamMember>>(teamMembers, HttpStatus.OK);
    }

    @RequestMapping(value = "/teamMembers/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeamMember> get(@PathVariable int id) {
        TeamMember teamMember = teamMemberRepository.findOne(id);
        if (teamMember == null) {
            return new ResponseEntity<TeamMember>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<TeamMember>(teamMember, HttpStatus.OK);
    }

    @RequestMapping(value = "/teamMembers", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<TeamMember>> create(@RequestBody TeamMember[] teamMembers, @RequestParam("newGameId") int gameId) {
        Game game = gameRepository.findOne(gameId);
        if (game == null) {
            return new ResponseEntity<ArrayList<TeamMember>>(HttpStatus.NOT_FOUND);
        }
        teamMemberServicesWorker.createTeamMembers(teamMembers, game);
        return new ResponseEntity<ArrayList<TeamMember>>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/teamMembers/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TeamMember> update(@PathVariable("id") int id, @RequestBody TeamMember teamMember) {
        TeamMember current = teamMemberRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<TeamMember>(HttpStatus.NOT_FOUND);
        }
        teamMember.setId(id);
        teamMemberRepository.save(teamMember);
        return new ResponseEntity<TeamMember>(current, HttpStatus.OK);
    }

    @RequestMapping(value = "/teamMembers/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        TeamMember teamMember = teamMemberRepository.findOne(id);
        if (teamMember == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        teamMemberRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
