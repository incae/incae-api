package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import com.pernix.incae.data.GameRepository;
import com.pernix.incae.services.pojos.ResponseCSV;
import com.pernix.incae.services.workers.GameServicesWorker;
import com.pernix.incae.services.workers.GamesResultsServicesWorker;

@RestController
public class GamesResultsServices {

    private final GamesResultsServicesWorker gamesResultsServicesWorker;

    @Autowired
    public GamesResultsServices(GameRepository gameRepository, GameServicesWorker gameServicesWorker,
            GamesResultsServicesWorker gamesResultsServicesWorker) {
        this.gamesResultsServicesWorker = gamesResultsServicesWorker;
    }

    @RequestMapping(value = "/games-results", method = RequestMethod.GET)
    public ResponseEntity<Void> getResults(HttpServletResponse response) throws IOException {
        ResponseCSV dataResult = gamesResultsServicesWorker.generateCSV();

        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=" + dataResult.getFilename());

        OutputStream writer = response.getOutputStream();

        writer.write(dataResult.getHeader().getBytes());
        writer.write(dataResult.getData().getBytes());
        writer.close();

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
