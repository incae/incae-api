package com.pernix.incae.services.pojos;

import java.util.Set;

import com.pernix.incae.entities.EmployeeInfo;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;

public class DecisionRequest {
    private Game game;
    private Tactic tactic;
    private Set<EmployeeInfo> selectedEmployees;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Tactic getTactic() {
        return tactic;
    }

    public void setTactic(Tactic tactic) {
        this.tactic = tactic;
    }

    public Set<EmployeeInfo> getSelectedEmployees() {
        return selectedEmployees;
    }

    public void setSelectedEmployees(Set<EmployeeInfo> selectedEmployees) {
        this.selectedEmployees = selectedEmployees;
    }

}
