package com.pernix.incae.services.pojos;

public class DecisionResultResponse {

    private String name;
    private Double gainScore;
    private Integer position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getGainScore() {
        return gainScore;
    }

    public void setGainScore(Double gainScore) {
        this.gainScore = gainScore;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
