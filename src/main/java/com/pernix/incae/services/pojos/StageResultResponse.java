package com.pernix.incae.services.pojos;

import java.util.Set;

public class StageResultResponse {

    private String name;
    private int id;
    private Set<DecisionResultResponse> decisions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
    	this.id = id;
    }

    public int getId() {
        return id;
    }

    public Set<DecisionResultResponse> getDecisions() {
        return decisions;
    }

    public void setDecisions(Set<DecisionResultResponse> decisions) {
        this.decisions = decisions;
    }
}
