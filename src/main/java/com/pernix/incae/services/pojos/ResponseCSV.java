package com.pernix.incae.services.pojos;

public class ResponseCSV {
    private String header;
    private String data;
    private String filename;

    public ResponseCSV() {
        this("","","");
    }

    public ResponseCSV(String header, String data, String filename) {
        this.header = header;
        this.data = data;
        this.filename = filename;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getHeader() {
        return header;
    }

    public String getData() {
        return data;
    }

    public String getFilename() {
        return filename;
    }
}