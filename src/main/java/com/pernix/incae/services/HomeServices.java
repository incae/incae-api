package com.pernix.incae.services;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeServices {

    @RequestMapping("/")
    public String home() {
        return "home";
    }
}
