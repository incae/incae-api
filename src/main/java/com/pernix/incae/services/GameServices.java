package com.pernix.incae.services;

import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.UserRepository;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.entities.Users;
import com.pernix.incae.services.workers.BoardServicesWorker;
import com.pernix.incae.services.workers.GameServicesWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameServices {

    private final GameRepository gameRepository;
    private final GameServicesWorker gameServicesWorker;
    private final UserRepository userRepository;
    private final BoardServicesWorker boardServicesWorker;

    @Autowired
    public GameServices(GameRepository gameRepository, GameServicesWorker gameServicesWorker,
                        UserRepository userRepository, BoardServicesWorker boardServicesWorker) {
        this.gameRepository = gameRepository;
        this.gameServicesWorker = gameServicesWorker;
        this.userRepository = userRepository;
        this.boardServicesWorker = boardServicesWorker;
    }

    @RequestMapping(value = "/games", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Game>> getAll() {
        Iterable<Game> games = gameRepository.findAllByOrderByCulturalChangeScoreDescBudgetDescAvailableDaysDesc();
        if (games == null) {
            return new ResponseEntity<Iterable<Game>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Iterable<Game>>(games, HttpStatus.OK);
    }

    @RequestMapping(value = "/games/{id}", method = RequestMethod.GET)
    public ResponseEntity<Game> get(@PathVariable int id) {
        Game game = gameRepository.findOne(id);
        if (game == null) {
            return new ResponseEntity<Game>(HttpStatus.NOT_FOUND);
        }
        gameServicesWorker.updateLastVisit(game);
        return new ResponseEntity<Game>(game, HttpStatus.OK);
    }

    @RequestMapping(value = "/games", method = RequestMethod.POST)
    public ResponseEntity<Game> create(@RequestBody Game game) {
        if (gameRepository.existsByName(game.getName())) {
            return new ResponseEntity<Game>(HttpStatus.CONFLICT);
        }
        gameServicesWorker.createGame(game);
        return new ResponseEntity<Game>(game, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/games/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        Game game = gameRepository.findOne(id);
        if (game == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        boardServicesWorker.deleteBoardsByGame(game);
        gameRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/game-by-username/{login}", method = RequestMethod.POST)
    public ResponseEntity<Game> getGameByUsername(@PathVariable("login") String login) {
        Users user = userRepository.findByLogin(login);
        if (user == null) {
            return new ResponseEntity<Game>(HttpStatus.NOT_ACCEPTABLE);
        }
        if (user.isPasswordLapsed()) {
            return new ResponseEntity<Game>(HttpStatus.GONE);
        }
        Game game = gameRepository.findByUser(user);
        if (game == null) {
            return new ResponseEntity<Game>(HttpStatus.NOT_FOUND);
        }
        gameServicesWorker.updateLastVisit(game);
        return new ResponseEntity<Game>(game, HttpStatus.OK);
    }

    @RequestMapping(value = "/games/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<Void> updateGameName(@PathVariable("id") int id, @RequestBody String newName) {
        Game game = gameRepository.findOne(id);
        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!gameServicesWorker.updateGameName(game, newName)) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/games/updateStatus/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<Void> updateGameStatus(@PathVariable("id") int id, @RequestBody String status) {
        Game game = gameRepository.findOne(id);
        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!gameServicesWorker.updateGameStatus(game, status)) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/games/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Game> updateGameDaysAndBudget(@PathVariable("id") int id, @RequestBody Tactic tactic) {
        Game game = gameRepository.findOne(id);
        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        game.setBudget(game.getBudget()-tactic.getPrice());
        game.setAvailableDays(game.getAvailableDays()-tactic.getDays());
        gameRepository.save(game);
        return new ResponseEntity<Game>(game, HttpStatus.OK);
    }

}
