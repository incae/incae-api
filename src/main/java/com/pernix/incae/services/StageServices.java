package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.StageRepository;
import com.pernix.incae.entities.Stage;

@RestController
public class StageServices {

    private final StageRepository stageRepository;

    @Autowired
    public StageServices(StageRepository stageRepository) {
        this.stageRepository = stageRepository;
    }

    @RequestMapping(value = "/stages", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Stage>> getAll() {
        Iterable<Stage> stages = stageRepository.findAll();
        if (stages == null) {
            return new ResponseEntity<Iterable<Stage>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Stage>>(stages, HttpStatus.OK);
    }

    @RequestMapping(value = "/stages/{id}", method = RequestMethod.GET)
    public ResponseEntity<Stage> get(@PathVariable int id) {
        Stage stage = stageRepository.findOne(id);
        if (stage == null) {
            return new ResponseEntity<Stage>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Stage>(stage, HttpStatus.OK);
    }
}
