package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.UserRepository;
import com.pernix.incae.entities.Users;

@RestController
public class UserServices {

    private final UserRepository userRepository;

    @Autowired
    public UserServices(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Users>> getAll() {
        Iterable<Users> users = userRepository.findAll();
        if (users == null) {
            return new ResponseEntity<Iterable<Users>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Users>>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Users> create(@RequestBody Users newUser) {
        Users user = userRepository.findByLogin(newUser.getLogin());
        if (user != null && user.getRole().getName().equals("ADMIN")) {
            return new ResponseEntity<Users>(HttpStatus.CONFLICT);
        }
        if (user != null && user.getRole().getName().equals("GUEST")) {
            return new ResponseEntity<>(HttpStatus.LOCKED);
        }
        userRepository.save(newUser);
        return new ResponseEntity<Users>(user, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        Users user = userRepository.findOne(id);
        if (user == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        userRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/users-by-role/{name}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Users>> getAllUsersByRoleName(@PathVariable("name") String roleName) {
        Iterable<Users> users = userRepository.findByRoleName(roleName);
        if (users == null) {
            return new ResponseEntity<Iterable<Users>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Users>>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/user-validation/{login}", method = RequestMethod.POST)
    public ResponseEntity<Void> userRoleValidation(@PathVariable("login") String login, @RequestBody String[] roles) {
        Users user = userRepository.findByLogin(login);
        if(user != null) {
            for (String roleName : roles) {
                if (user.getRole().getName().equals(roleName)) {
                    return new ResponseEntity<Void>(HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
    }
}
