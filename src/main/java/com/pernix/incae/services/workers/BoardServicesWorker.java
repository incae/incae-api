package com.pernix.incae.services.workers;

import com.pernix.incae.data.BoardRepository;
import com.pernix.incae.entities.Board;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;
import java.util.HashMap;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoardServicesWorker {

    private final BoardRepository boardRepository;

    @Autowired
    public BoardServicesWorker( BoardRepository boardRepository) {
        this.boardRepository = boardRepository;
    }

    public Board createBoard(Board request) {
        Game game = request.getGame();
        Tactic tactic = request.getTactic();

        Board currentBoard = new Board();

        currentBoard.setPosX(request.getPosX());
        currentBoard.setPosY(request.getPosY());
        currentBoard.setGame(game);
        currentBoard.setTactic(tactic);
        boardRepository.save(currentBoard);

        return currentBoard;
    }

    public boolean updatePosY(Board board, Integer posY) {
        board.setPosY(posY);
        boardRepository.save(board);
        return true;
    }

    public boolean updateIsApplied(Board board, Boolean isApplied) {
        board.setIsApplied(isApplied);
        boardRepository.save(board);
        return true;
    }

    public Iterable<Board> getNotAppliedBoards(Game game) {
        HashMap<Integer, Board> results = new HashMap<Integer, Board>();

        Set<Board> boards = boardRepository.findAllByGame(game);

        for (Board board : boards) {
            if(!board.getIsApplied()){
                results.put(board.getId(), board);
            }
        }

        return results.values();
    }

    public void deleteBoardsByGame(Game game) {
        Iterable<Board> results = boardRepository.findAllByGame(game)
                .stream()
                .collect(Collectors.toMap(Board::getId, Function.identity()))
                .values();
        
        boardRepository.delete(results);
    }    
}
