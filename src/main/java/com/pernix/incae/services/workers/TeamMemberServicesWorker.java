package com.pernix.incae.services.workers;

import com.pernix.incae.data.TeamMemberRepository;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.TeamMember;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamMemberServicesWorker {
    private final TeamMemberRepository teamMemberRepository;

    @Autowired
    public TeamMemberServicesWorker(TeamMemberRepository teamMemberRepository) {
        this.teamMemberRepository = teamMemberRepository;
    }

    public void createTeamMembers(TeamMember[] teamMembers, Game game) {
        for (TeamMember teamMember : teamMembers) {
            teamMember.setGame(game);
            teamMemberRepository.save(teamMember);
        }
    }
}