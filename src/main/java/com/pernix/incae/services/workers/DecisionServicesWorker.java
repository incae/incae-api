package com.pernix.incae.services.workers;

import com.pernix.incae.AppConfig;
import com.pernix.incae.data.DecisionRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeInfoRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.entities.Decision;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.Employee;
import com.pernix.incae.entities.EmployeeInfo;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Stage;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.entities.util.GameStatus;
import com.pernix.incae.entities.util.ScoreLevel;
import com.pernix.incae.entities.util.TacticClassification;
import com.pernix.incae.services.pojos.DecisionResultResponse;
import com.pernix.incae.services.pojos.StageResultResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service
public class DecisionServicesWorker {

    private final GameRepository gameRepository;
    private final TacticRepository tacticRepository;
    private final DecisionRepository decisionRepository;
    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final StageRepository stageRepository;
    private final EmployeeInfoRepository employeeInfoRepository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    public DecisionServicesWorker(GameRepository gameRepository, TacticRepository tacticRepository,
            DecisionRepository decisionRepository, EmployeeRepository employeeRepository,
            DepartmentRepository departmentRepository, StageRepository stageRepository,
            EmployeeInfoRepository employeeInfoRepository) {
        this.gameRepository = gameRepository;
        this.tacticRepository = tacticRepository;
        this.decisionRepository = decisionRepository;
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.stageRepository = stageRepository;
        this.employeeInfoRepository = employeeInfoRepository;
    }

    public void createDecision(Decision request) {
        Game game = request.getGame();
        Tactic tactic = request.getTactic();
        Set<Decision> decisions = decisionRepository.findAllByGame(game);
        Set<Decision> mandatoryDecisions = decisionRepository.findByGameAndClassification(game, TacticClassification.MANDATORY );
        ScoreLevel scoreLevel = ScoreLevel.NONE;

        Decision currentDecision = new Decision();
        int currentIdealPosition = tactic.getIdealPosition();
        ArrayList<Integer> prerequisiteTactics = getPrerequisiteArray(currentIdealPosition);
        int unmetPrereq = getUnmetPrerequisites(prerequisiteTactics, currentIdealPosition, decisions);

        int previousUnmetPrereq = 0;
        if ( decisions.size() > 0 ) {
            int previousIdealPosition = decisionRepository.findFirstByGameOrderByPositionDesc(game).getTactic().getIdealPosition();
            previousUnmetPrereq = getUnmetPrerequisites(getPrerequisiteArray(previousIdealPosition), previousIdealPosition, decisions);
        }

        int decisionPosition = mandatoryDecisions.size() + 1;
        int samePositionTactics = tacticRepository.findByIdealPosition(tactic.getIdealPosition()).size();

        currentDecision.setPosX(request.getPosX());
        currentDecision.setPosition(decisionPosition);
        currentDecision.setClassification(tactic.getClassification());
        currentDecision.setGainScore(0.0);
        currentDecision.setGame(game);
        currentDecision.setTactic(tactic);
        currentDecision.setScoreLevel(scoreLevel);
        currentDecision.setSelectedEmployees(request.getSelectedEmployees());
        decisionRepository.save(currentDecision);

        Double previousGameCulturalChangeScore = new Double(game.getCulturalChangeScore());

        HashMap<Integer, Boolean> involvedLevels = getInvolvedHierarchicalLevels(currentDecision);
        Set<Department> departments = departmentRepository.findAllByGame(game);

        Double totalGameLeadershipChangeScore = 0.0;
        int countGameEmployees = 0;
        for (Department department : departments) {
            Set<Employee> employees = employeeRepository.findAllByDepartment(department);

            Double totalDepartmentLeadershipChangeScore = 0.0;
            for (Employee employee : employees) {
                updateEmployee(employee, tactic, involvedLevels);

                totalDepartmentLeadershipChangeScore += employee.getCommitmentLevel();
                totalGameLeadershipChangeScore += employee.getCommitmentLevel();
                countGameEmployees++;
            }

            Double departmentLeadershipChangeScore = employees.size() > 0
                    ? totalDepartmentLeadershipChangeScore / employees.size()
                    : 0;
            updateDepartment(department, departmentLeadershipChangeScore);
        }

        Double gameLeadershipChangeScore = countGameEmployees > 0 ? totalGameLeadershipChangeScore / countGameEmployees
                : 0;
        
        scoreLevel = scoreLevelOnCurrent(tactic, decisionPosition, samePositionTactics, unmetPrereq, previousUnmetPrereq);
        updateGame(game, tactic, gameLeadershipChangeScore, scoreLevel);
        currentDecision.setScoreLevel(scoreLevel);
        currentDecision.setGainScore(game.getCulturalChangeScore() - previousGameCulturalChangeScore);
        decisionRepository.save(currentDecision);
    }


    private ArrayList<Integer> getPrerequisiteArray(int currentIdealPosition) {
        ArrayList<Integer> prerequisiteTactics = new ArrayList<Integer>();
        // Create Prerequisites Array for Current Decision
        for (Tactic tacticElement : tacticRepository.findAll()) {
            int tacticElementPosition = tacticElement.getIdealPosition();
            Boolean isMandatory = tacticElement.getClassification() == TacticClassification.MANDATORY;
            if (isMandatory & tacticElementPosition < currentIdealPosition) {
                // System.out.println(tacticElement.getIdealPosition());
                prerequisiteTactics.add(tacticElement.getIdealPosition());
            }
        }
        return prerequisiteTactics;
    }

    private int getUnmetPrerequisites(ArrayList<Integer> prerequisiteTactics, int currentIdealPosition,
            Set<Decision> decisions) {
        ArrayList<Integer> previousDecisionsPositions = new ArrayList<Integer>();
        // Check Prerequisites Array for Decisions taken
        for (Decision previousDecision : decisions) {
            if (prerequisiteTactics.size() == 0)
                break;
            int previousDecisionPosition = previousDecision.getTactic().getIdealPosition();
            previousDecisionsPositions.add(previousDecisionPosition);
            int index = 0;
            // System.out.println("Checking Prereqs For: "+previousDecisionPosition);
            for (int prerequisite : prerequisiteTactics) {
                // System.out.println(index);
                if (prerequisite == previousDecisionPosition) {
                    prerequisiteTactics.remove(index);
                    // System.out.println("Removed: "+prerequisite);
                    // System.out.println("New Array: "+prerequisiteTactics);
                    break;
                }
                index++;
            }
        }
        // System.out.println("Decisions Array: "+previousDecisionsPositions);
        int unmetPrereq = prerequisiteTactics.size();
        return unmetPrereq;
    }

    private HashMap<Integer, Boolean> getInvolvedHierarchicalLevels(Decision decision) {
        HashMap<Integer, Boolean> levels = new HashMap<Integer, Boolean>();
        levels.put(0, false);
        levels.put(1, false);
        levels.put(2, false);
        levels.put(3, false);

        for (EmployeeInfo employee : decision.getSelectedEmployees()) {
            EmployeeInfo employeeInfo = employeeInfoRepository.findOne(employee.getId());
            Integer level = employeeInfo.getHierarchicalLevel();
            levels.put(level, true);
        }
        return levels;
    }

    private ScoreLevel scoreLevelOnCurrent(Tactic tactic, int decisionPosition, int samePositionTactics,
            int unmetPrereq, int previousUnmetPrereq) {
        int padding = samePositionTactics - 1;
        int offset = Math.abs(tactic.getIdealPosition() - decisionPosition);
        if (tactic.getClassification() == TacticClassification.NEGATIVE) {
            return ScoreLevel.NEGATIVE;
        } else {
            if (offset >= 5 + padding) {
                return ScoreLevel.NONE;
            } else if (unmetPrereq >= 3) {
                return ScoreLevel.LOW;
            } else if (unmetPrereq == 1 || unmetPrereq == 2) {
                return ScoreLevel.MEDIUM;
            } else if (unmetPrereq == 0 & previousUnmetPrereq != 0) {
                return ScoreLevel.HIGH;
            } else if ((offset >= 0 & offset <= padding) | (unmetPrereq == 0 & previousUnmetPrereq == 0)) {
                return ScoreLevel.PERFECT;
            } else {
                return ScoreLevel.NONE;
            }
        }
    }

    private void updateGame(Game game, Tactic tactic, Double gameLeadershipChangeScore, ScoreLevel scoreLevel) {
        game.setAvailableDays(game.getAvailableDays() - tactic.getDays());
        game.setBudget(game.getBudget() - tactic.getPrice());
        game.setLeadershipChangeScore(gameLeadershipChangeScore);

        double newCulturalChangeScore = game.getCulturalChangeScore();

        double scoreChange = 0.0;
        switch (scoreLevel) {
            case NEGATIVE:
                scoreChange = tactic.getTacticScore().getNegative();
                break;
            case PERFECT:
                scoreChange = tactic.getTacticScore().getPerfect();
                break;
            case HIGH:
                scoreChange = tactic.getTacticScore().getHigh();
                break;
            case MEDIUM:
                scoreChange = tactic.getTacticScore().getMedium();
                break;
            case LOW:
                scoreChange = tactic.getTacticScore().getLow();
                break;
            case NONE:
                scoreChange = 0.0;
                break;
        }

        newCulturalChangeScore += scoreChange;
        newCulturalChangeScore = (newCulturalChangeScore <= 100 ? newCulturalChangeScore : 100);
        newCulturalChangeScore = (newCulturalChangeScore >= 0 ? newCulturalChangeScore : 0);

        game.setCulturalChangeScore(newCulturalChangeScore);
        gameRepository.save(game);

        updateGameStatus(game);
        updateGameStage(game, tactic);
    }

    private void updateGameStage(Game game, Tactic tactic) {

        game.setSpentDaysOnCurrentStage((int) (game.getSpentDaysOnCurrentStage() + tactic.getDays()));

        boolean isInLastStage = game.getCurrentStage().getNextStage() == null;
        boolean exceededMaxDays = game.getSpentDaysOnCurrentStage() >= game.getCurrentStage().getMaximumDays();
        boolean exceededMinDays = game.getSpentDaysOnCurrentStage() >= game.getCurrentStage().getMinimumDays();
        boolean wrongStageTactic = !tactic.getStage().equals(game.getCurrentStage());

        if (!isInLastStage && (exceededMaxDays || (exceededMinDays && wrongStageTactic))) {
            game.setCurrentStage(game.getCurrentStage().getNextStage());
            game.setSpentDaysOnCurrentStage(0);
        }

        gameRepository.save(game);
    }

    private void updateGameStatus(Game game) {
        Iterable<Tactic> tactics = tacticRepository.findAll();
        Set<Decision> decisions = decisionRepository.findAllByGame(game);
        boolean canContinuePlaying = false;
        for (Tactic tacticTemp : tactics) {
            boolean isApplied = false;
            for (Decision decision : decisions) {
                if (decision.getTactic().getId() == tacticTemp.getId()) {
                    isApplied = true;
                    break;
                }
            }
            if (!isApplied && tacticTemp.getPrice() <= game.getBudget()
                    && tacticTemp.getDays() <= game.getAvailableDays() && game.getCulturalChangeScore() <= 69) {
                canContinuePlaying = true;
                break;
            }
        }

        if (!canContinuePlaying) {
            if (((game.getCulturalChangeScore() >= appConfig.getIncaeWinPercentage())
                    || (game.getLeadershipChangeScore() >= appConfig.getIncaeWinPercentage()))
                    || game.getCulturalChangeScore() == 69) {
                game.setStatus(GameStatus.FINISHED_WON);
            } else {
                game.setStatus(GameStatus.FINISHED_LOST);
            }
            gameRepository.save(game);
        }
    }

    private void updateDepartment(Department department, Double departmentAverageScore) {
        department.setLeadershipChangeScore(departmentAverageScore);
        departmentRepository.save(department);
    }

    private void updateEmployee(Employee employee, Tactic tactic, HashMap<Integer, Boolean> involvedLevels) {

        TacticClassification classification = tactic.getClassification();

        boolean employeeBelongsToInvolvedLevels = involvedLevels.get(employee.getEmployeeInfo().getHierarchicalLevel());
        double newCommitmentLevel = employee.getCommitmentLevel();

        switch (classification) {
            case MANDATORY:
                if (employeeBelongsToInvolvedLevels) {
                    newCommitmentLevel += 10;
                }
                break;
            case OPTIONAL:
                if (employeeBelongsToInvolvedLevels) {
                    newCommitmentLevel += 5;
                }
                break;
            case NEGATIVE:
                newCommitmentLevel -= 5;
                break;
        }

        newCommitmentLevel = (newCommitmentLevel <= 100 ? newCommitmentLevel : 100);
        newCommitmentLevel = (newCommitmentLevel >= 0 ? newCommitmentLevel : 0);

        employee.setCommitmentLevel(newCommitmentLevel);
        employeeRepository.save(employee);
    }

    public Double precisionOnDecisionPosition(Tactic tactic, int decisionPosition) {
        Double totalTacticsAvailable = (double) tacticRepository.count();
        Double positionPrecision = (totalTacticsAvailable - Math.abs(tactic.getIdealPosition() - decisionPosition))
                / totalTacticsAvailable;
        return positionPrecision;
    }

    public Iterable<StageResultResponse> getDecisionsByStage(Game game) {
        HashMap<Integer, StageResultResponse> results = new HashMap<Integer, StageResultResponse>();

        Set<Decision> decisions = decisionRepository.findAllByGame(game);
        Iterable<Stage> stages = stageRepository.findAll();

        for (Stage stage : stages) {
            StageResultResponse stageResult = new StageResultResponse();
            stageResult.setName(stage.getName());
            stageResult.setId(stage.getId());
            Set<DecisionResultResponse> decisionResults = new HashSet<DecisionResultResponse>();
            stageResult.setDecisions(decisionResults);

            results.put(stage.getId(), stageResult);
        }

        for (Decision decision : decisions) {
            DecisionResultResponse decisionResult = new DecisionResultResponse();
            decisionResult.setName(decision.getTactic().getName());
            decisionResult.setGainScore(decision.getGainScore());
            decisionResult.setPosition(decision.getPosition());

            results.get(decision.getTactic().getStage().getId()).getDecisions().add(decisionResult);
        }

        return results.values();
    }

    public Iterable<StageResultResponse> getAllDecisionsByStage() {
        HashMap<Integer, StageResultResponse> results = new HashMap<Integer, StageResultResponse>();

        Iterable<Decision> decisions = consolidatedWeaknessResult();
        Iterable<Stage> stages = stageRepository.findAll();

        for (Stage stage : stages) {
            StageResultResponse stageResult = new StageResultResponse();
            stageResult.setName(stage.getName());
            stageResult.setId(stage.getId());
            Set<DecisionResultResponse> decisionResult = new HashSet<DecisionResultResponse>();
            stageResult.setDecisions(decisionResult);

            results.put(stage.getId(), stageResult);
        }

        for (Decision decision : decisions) {
            DecisionResultResponse decisionResult = new DecisionResultResponse();
            decisionResult.setName(decision.getTactic().getName());
            decisionResult.setGainScore(decision.getGainScore());
            decisionResult.setPosition(decision.getPosition());

            results.get(decision.getTactic().getStage().getId()).getDecisions().add(decisionResult);
        }

        return results.values();
    }

    public Iterable<Decision> consolidatedWeaknessResult() {
        HashMap<Integer, Decision> results = new HashMap<Integer, Decision>();
        Iterable<Decision> decisions = decisionRepository.findAll();

        for (Decision decision : decisions) {
            if (decision.getGainScore() <= 0) {
                if (!results.containsKey(decision.getTactic().getId())) {
                    Decision decisionResult = new Decision();
                    decisionResult.setGainScore(decision.getGainScore());
                    decisionResult.setGame(decision.getGame());
                    decisionResult.setId(decision.getId());
                    decisionResult.setPosition(decision.getPosition());
                    decisionResult.setSelectedEmployees(decision.getSelectedEmployees());
                    decisionResult.setTactic(decision.getTactic());

                    results.put(decisionResult.getTactic().getId(), decisionResult);
                } else {
                    Decision currentDecision = results.get(decision.getTactic().getId());
                    currentDecision.setGainScore(currentDecision.getGainScore() + decision.getGainScore());
                    results.replace(currentDecision.getTactic().getId(), currentDecision);
                }
            }
        }

        return results.values();
    }
}
