package com.pernix.incae.services.workers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.pernix.incae.services.pojos.ResponseCSV;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.entities.Decision;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.TeamMember;

@Service
public class GamesResultsServicesWorker {

    private final GameRepository gameRepository;

    @Autowired
    public GamesResultsServicesWorker(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public String gamesResults() {
        Iterable<Game> games = gameRepository.findAll();
        StringBuilder line = new StringBuilder();
        for (Game game : games) {
            boolean isFirstLine = true;
            Iterator<TeamMember> itTeamMember = game.getTeamMembers().iterator();

            List<Decision> decisions = sortDecisions(game.getDecisions());
            Iterator<Decision> itDecision = decisions.iterator();

            line.append(System.getProperty("line.separator"));
            line.append(game.getName());
            while (itTeamMember.hasNext() || itDecision.hasNext()) {
                Decision decision = (itDecision.hasNext() ? itDecision.next() : null);
                TeamMember member = (itTeamMember.hasNext() ? itTeamMember.next() : null);

                if (isFirstLine) {
                    line.append(firstLine(game, member, decision));
                } else if (decision != null && member != null) {
                    line.append(decisionsAndMembersLine(member, decision));
                } else if (decision != null && member  == null) {
                    line.append(decisionsLine(decision));
                } else if (member != null && decision == null) {
                    line.append(membersLine(member));
                }

                line.append(System.getProperty("line.separator"));
                isFirstLine = false;
            }
        }
        return line.toString();
    }

    public String tacticName(Decision decision) { 
        StringBuilder line = new StringBuilder();
        line.append((decision != null ? decision.getTactic().getName().replace(',', ' ') : ",") + ",");
        return line.toString();
    }

    public String decisionInFirstLine(Decision decision) { 
        StringBuilder line = new StringBuilder();
        line.append(tacticName(decision) + (decision != null ? decision.getTactic().getStage().getName() : ",") + "," + (decision != null ? decision.getGainScore() : ","));
        return line.toString();
    }

    public String firstLine(Game game, TeamMember member, Decision decision) { 
        StringBuilder line = new StringBuilder();
        line.append("," + (member != null ? member.getName() : ",") + "," + game.getBudget() + "," + game.getAvailableDays() + "," + game.getCulturalChangeScore() + ","
                        + String.format("%.2f", game.getLeadershipChangeScore()) + ","
                        + decisionInFirstLine(decision));
        return line.toString();
    }

    public String decisionsAndMembersLine(TeamMember member, Decision decision) { 
        StringBuilder line = new StringBuilder();
        line.append("," + member.getName() + ",,,,," + tacticName(decision) + decision.getTactic().getStage().getName() + "," + decision.getGainScore());
        return line.toString();
    }

    public String decisionsLine(Decision decision) { 
        StringBuilder line = new StringBuilder();
        line.append(",,,,,," + tacticName(decision) + decision.getTactic().getStage().getName() + "," + decision.getGainScore());
        return line.toString();
    }

    public String membersLine(TeamMember member) { 
        StringBuilder line = new StringBuilder();
        line.append("," + member.getName() + ",,,,,,,");
        return line.toString();
    }

    private List<Decision> sortDecisions(Set<Decision> decisions) {
        List<Decision> sortedDecisions = fillDecisionsList(decisions);
        sortedDecisions.sort((left, right) -> left.getId() - right.getId());
        return sortedDecisions;
    }

    private List<Decision> fillDecisionsList(Set<Decision> decisions) {
        List<Decision> decisionsList = new ArrayList<>();
        for (Decision decision : decisions) {
            decisionsList.add(decision);
        }
        return decisionsList;
    }

    public String getResultsHeaders() {
        String headerLine = "";
        ArrayList<String> headers = new ArrayList<String>();
        headers.add("Nombre del Equipo");
        headers.add("Miembros");
        headers.add("Presupuesto disponible");
        headers.add("Días disponibles");
        headers.add("Puntaje Cultura de Cambio");
        headers.add("Puntaje Liderazgo de Cambio");
        headers.add("Decisiones tomadas");
        headers.add("Etapa a la que pertenece la Decisión");
        headers.add("Puntos obtenidos de la decisión");
        headerLine = String.join(",", headers);
        return headerLine;
    }

    public ResponseCSV generateCSV() {
        String headers = this.getResultsHeaders();
        String csvData = this.gamesResults();
        String fileName = "Resultados.csv";
        ResponseCSV response = new ResponseCSV(headers, csvData, fileName);
        return response;
    }
}