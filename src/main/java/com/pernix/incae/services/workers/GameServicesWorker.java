package com.pernix.incae.services.workers;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pernix.incae.AppConfig;
import com.pernix.incae.data.DepartmentInfoRepository;
import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeInfoRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.RoleRepository;
import com.pernix.incae.data.StageRepository;
import com.pernix.incae.data.UserRepository;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.DepartmentInfo;
import com.pernix.incae.entities.Employee;
import com.pernix.incae.entities.EmployeeInfo;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.util.GameStatus;
import com.pernix.incae.entities.Users;
import com.pernix.incae.entities.Role;

@Service
public class GameServicesWorker {

    private final GameRepository gameRepository;
    private final EmployeeInfoRepository employeeInfoRepository;
    private final EmployeeRepository employeeRepository;
    private final DepartmentInfoRepository departmentInfoRepository;
    private final DepartmentRepository departmentRepository;
    private final StageRepository stageRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    public GameServicesWorker(GameRepository gameRepository, EmployeeInfoRepository employeeInfoRepository,
            EmployeeRepository employeeRepository, DepartmentInfoRepository departmentInfoRepository,
            DepartmentRepository departmentRepository, StageRepository stageRepository,
            RoleRepository roleRepository, UserRepository userRepository) {
        this.gameRepository = gameRepository;
        this.employeeInfoRepository = employeeInfoRepository;
        this.employeeRepository = employeeRepository;
        this.departmentInfoRepository = departmentInfoRepository;
        this.departmentRepository = departmentRepository;
        this.stageRepository = stageRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public void createGame(Game game) {
        Role role = roleRepository.findById(3);
        game.setAvailableDays(appConfig.getIncaeGameAvailableDays());
        game.setBudget(appConfig.getIncaeGameBudget());
        game.setCulturalChangeScore(0.0);
        game.setLeadershipChangeScore(0.0);
        game.setLastVisit(new Date());
        game.setStatus(GameStatus.IN_PROGRESS);
        game.setSpentDaysOnCurrentStage(0);

        game.setCurrentStage(stageRepository.findOne(1));

        Users user = game.getUser();
        user.setRole(role);
        user.setCreatedAt(new Date());
        userRepository.save(user);
        game.setUser(user);

        gameRepository.save(game);

        createDepartmentsFor(game);
    }

    public void updateLastVisit(Game game) {
        game.setLastVisit(new Date());
        gameRepository.save(game);
    }

    private void createDepartmentsFor(Game game) {
        Iterable<DepartmentInfo> departmentsInfo = departmentInfoRepository.findAll();
        int totalEmployees = 0;
        double totalLeadershipScore = 0.0;
        for (DepartmentInfo departmentInfo : departmentsInfo) {
            Department department = new Department();
            department.setDepartmentInfo(departmentInfo);
            department.setGame(game);
            department.setLeadershipChangeScore(0.0);

            departmentRepository.save(department);

            createEmployeesFor(department);

            totalEmployees += department.getEmployees().size();
            totalLeadershipScore += department.getLeadershipChangeScore() * department.getEmployees().size();
        }

        game.setLeadershipChangeScore(totalLeadershipScore / totalEmployees);
        gameRepository.save(game);
    }

    private void createEmployeesFor(Department department) {
        Set<EmployeeInfo> employeesInfo = employeeInfoRepository
                .findAllByDepartmentInfoId(department.getDepartmentInfo().getId());

        double departmentLeadershipScoreTotal = 0.0;

        department.setEmployees(new HashSet<Employee>());

        for (EmployeeInfo employeeInfo : employeesInfo) {
            Employee employee = new Employee();
            employee.setEmployeeInfo(employeeInfo);
            employee.setDepartment(department);
            // Initial Commitment Level is 0-10. But commitment level is 0-100
            employee.setCommitmentLevel(employeeInfo.getInitialCommitmentLevel() * 10);

            departmentLeadershipScoreTotal += employee.getCommitmentLevel();

            employeeRepository.save(employee);

            department.getEmployees().add(employee);
        }

        department.setLeadershipChangeScore(departmentLeadershipScoreTotal / employeesInfo.size());
        departmentRepository.save(department);
    }

    public boolean updateGameName(Game game, String newName) {
        Users user = game.getUser();
        if (gameRepository.existsByName(newName)) {
            return false;
        }
        user.setLogin(newName);
        user.setName(newName);
        userRepository.save(user);
        game.setName(newName);
        gameRepository.save(game);
        return true;
    }

    public boolean updateGameStatus(Game game, String status) {
        switch (status) {
            case "FINISHED_LOST":
                game.setStatus(GameStatus.FINISHED_LOST);
                break;
            case "FINISHED_WON":
                game.setStatus(GameStatus.FINISHED_WON);
                break;
            case "IN_PROGRESS":
                game.setStatus(GameStatus.IN_PROGRESS);
                break;

            default:
                break;
        }
        gameRepository.save(game);
        return true;
    }
}
