package com.pernix.incae.services;

import com.pernix.incae.data.BoardRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.data.TacticRepository;
import com.pernix.incae.entities.Board;
import com.pernix.incae.entities.Game;
import com.pernix.incae.entities.Tactic;
import com.pernix.incae.services.workers.BoardServicesWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BoardServices {

    private final BoardRepository boardRepository;
    private final TacticRepository tacticRepository;
    private final GameRepository gameRepository;
    private final BoardServicesWorker boardServicesWorker;

    @Autowired
    public BoardServices(BoardRepository boardRepository, TacticRepository tacticRepository,
            GameRepository gameRepository, BoardServicesWorker BoardServicesWorker) {
        this.boardRepository = boardRepository;
        this.tacticRepository = tacticRepository;
        this.gameRepository = gameRepository;
        this.boardServicesWorker = BoardServicesWorker;

    }

    @RequestMapping(value = "/boards", method = RequestMethod.POST)
    public ResponseEntity<Board> create(@RequestBody Board board) {
        if (board.getGame() == null || board.getTactic() == null) {
            return new ResponseEntity<Board>(HttpStatus.BAD_REQUEST);
        }

        Game game = gameRepository.findOne(board.getGame().getId());
        Tactic tactic = tacticRepository.findOne(board.getTactic().getId());
        if (game == null || tactic == null) {
            return new ResponseEntity<Board>(HttpStatus.NOT_FOUND);
        }

        board.setGame(game);
        board.setTactic(tactic);

        Board existingBoard = boardRepository.findByGameAndTactic(game, tactic);
        if (existingBoard != null) {
            return new ResponseEntity<Board>(existingBoard, HttpStatus.OK);
        }

        if (game.getBudget() < tactic.getPrice() || game.getAvailableDays() < tactic.getDays()) {
            return new ResponseEntity<Board>(HttpStatus.PRECONDITION_FAILED);
        }
        
        boardServicesWorker.createBoard(board);
        Board result = boardRepository.findByGameAndTactic(game, tactic);
        return new ResponseEntity<Board>(result, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/boards", method = RequestMethod.PUT)
    public ResponseEntity<Board> update(@RequestBody Board board) {
        Board current = boardRepository.findOne(board.getId());
        if (current == null) {
            return new ResponseEntity<Board>(HttpStatus.NOT_FOUND);
        }

        Game game = gameRepository.findOne(current.getGame().getId());
        Tactic tactic = tacticRepository.findOne(current.getTactic().getId());
        if (game.getBudget() < tactic.getPrice() || game.getAvailableDays() < tactic.getDays()) {
            return new ResponseEntity<Board>(HttpStatus.PRECONDITION_FAILED);
        }

        current.setPosX(board.getPosX());
        current.setPosY(board.getPosY());
        boardRepository.save(current);
        
        Board result = boardRepository.findOne(board.getId());
        return new ResponseEntity<Board>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/boards/{id}/updatePosY", method = RequestMethod.PATCH)
    public ResponseEntity<Board> updatePosY(@PathVariable("id") int id, @RequestBody Integer newPosY) {
        Board current = boardRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<Board>(HttpStatus.NOT_FOUND);
        }
        if (!boardServicesWorker.updatePosY(current, newPosY)) {
            return new ResponseEntity<Board>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Board>(HttpStatus.OK);
    }

    @RequestMapping(value = "/boards/{id}/updateIsApplied", method = RequestMethod.PATCH)
    public ResponseEntity<Board> updateApply(@PathVariable("id") int id, @RequestBody Boolean isApplied) {
        Board current = boardRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<Board>(HttpStatus.NOT_FOUND);
        }
        if (!boardServicesWorker.updateIsApplied(current, isApplied)) {
            return new ResponseEntity<Board>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Board>(HttpStatus.OK);
    }

    @RequestMapping(value = "/boards", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Board>> getAllByGame(@RequestParam("gameId") int gameId) {
        Game game = gameRepository.findOne(gameId);
        if (game == null) {
            return new ResponseEntity<Iterable<Board>>(HttpStatus.NOT_FOUND);
        }

        Iterable<Board> boards = boardRepository.findAllByGameOrderByPosYAsc(game);
        if (boards == null) {
            return new ResponseEntity<Iterable<Board>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Iterable<Board>>(boards, HttpStatus.OK);
    }

    @RequestMapping(value = "/boards/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        Board current = boardRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        boardRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/boards/deleteNotAppliedBoards/{gameId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteNotAppliedDecisions(@PathVariable int gameId) {
        Game game = gameRepository.findOne(gameId);
        if (game == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        Iterable<Board> results = boardServicesWorker.getNotAppliedBoards(game);
        boardRepository.delete(results);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
