package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.GameRepository;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.Game;

@RestController
public class DepartmentServices {

    private final GameRepository gameRepository;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServices(GameRepository gameRepository, DepartmentRepository departmentRepository) {
        this.gameRepository = gameRepository;
        this.departmentRepository = departmentRepository;
    }

    @RequestMapping(value = "/departments", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Department>> getAll(@RequestParam("gameId") Integer gameId) {
        Game game = gameRepository.findOne(gameId);
        if (game == null) {
            return new ResponseEntity<Iterable<Department>>(HttpStatus.NOT_FOUND);
        }

        Iterable<Department> departments = departmentRepository.findAllByGame(game);
        if (departments == null) {
            return new ResponseEntity<Iterable<Department>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Department>>(departments, HttpStatus.OK);
    }

    @RequestMapping(value = "/departments/{id}", method = RequestMethod.GET)
    public ResponseEntity<Department> get(@PathVariable int id) {
        Department department = departmentRepository.findOne(id);
        if (department == null) {
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Department>(department, HttpStatus.OK);
    }
}
