package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.EmployeeInfoRepository;
import com.pernix.incae.entities.EmployeeInfo;

@RestController
public class EmployeeInfoServices {

    private final EmployeeInfoRepository employeeInfoRepository;

    @Autowired
    public EmployeeInfoServices(EmployeeInfoRepository employeeInfoRepository) {
        this.employeeInfoRepository = employeeInfoRepository;
    }

    @RequestMapping(value = "/employees_info", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EmployeeInfo>> getAll() {
        Iterable<EmployeeInfo> employees = employeeInfoRepository.findAll();

        if (employees == null) {
            return new ResponseEntity<Iterable<EmployeeInfo>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Iterable<EmployeeInfo>>(employees, HttpStatus.OK);
    }
}
