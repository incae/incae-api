package com.pernix.incae.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.incae.data.DepartmentRepository;
import com.pernix.incae.data.EmployeeRepository;
import com.pernix.incae.entities.Department;
import com.pernix.incae.entities.Employee;

@RestController
public class EmployeeServices {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public EmployeeServices(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Employee>> getAll(@RequestParam("departmentId") Integer departmentId) {
        Department department = departmentRepository.findOne(departmentId);

        if (department == null) {
            return new ResponseEntity<Iterable<Employee>>(HttpStatus.NOT_FOUND);
        }

        Iterable<Employee> employees = employeeRepository.findAllByDepartment(department);

        if (employees == null) {
            return new ResponseEntity<Iterable<Employee>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Employee>>(employees, HttpStatus.OK);
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.GET)
    public ResponseEntity<Employee> get(@PathVariable int id) {
        Employee employee = employeeRepository.findOne(id);
        if (employee == null) {
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }
}
