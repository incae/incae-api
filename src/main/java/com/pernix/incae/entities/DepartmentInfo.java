package com.pernix.incae.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DepartmentInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "departmentInfo")
    private Set<EmployeeInfo> employeesInfo;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "departmentInfo")
    private Set<Department> departments;

    public DepartmentInfo() {
    }

    public DepartmentInfo(DepartmentInfo departmentInfo) {
        this.id = departmentInfo.getId();
        this.name = departmentInfo.getName();
        this.employeesInfo = departmentInfo.getEmployeesInfo();
        this.departments = departmentInfo.getDepartments();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<EmployeeInfo> getEmployeesInfo() {
        return employeesInfo;
    }

    public void setEmployeesInfo(Set<EmployeeInfo> employeesInfo) {
        this.employeesInfo = employeesInfo;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }
}
