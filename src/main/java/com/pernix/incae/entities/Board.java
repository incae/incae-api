package com.pernix.incae.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull
    @Column(name = "pos_x", nullable = false)
    private Integer posX;

    @NotNull
    @Column(name = "pos_y", nullable = false)
    private Integer posY;

    @NotNull
    @Column(nullable = false, columnDefinition = "boolean default 'false'")
    private boolean isApplied;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_tacticId", nullable = false)
    private Tactic tactic;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_gameId", nullable = false)
    private Game game;

    public Board(){ }

    public Board(Board board) {
        this.id = board.getId();
        this.tactic = board.getTactic();
        this.game = board.getGame();
        this.posX = board.getPosX();
        this.posY = board.getPosY();
        this.isApplied = board.getIsApplied();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPosX() {
        return posX;
    }

    public void setPosX(Integer posX) {
        this.posX = posX;
    }

    public Integer getPosY() {
        return posY;
    }

    public void setPosY(Integer posY) {
        this.posY = posY;
    }

    public boolean getIsApplied() {
        return isApplied;
    }

    public void setIsApplied(boolean isApplied) {
        this.isApplied = isApplied;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Tactic getTactic() {
        return tactic;
    }

    public void setTactic(Tactic tactic) {
        this.tactic = tactic;
    }
}
