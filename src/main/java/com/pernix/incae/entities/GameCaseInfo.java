package com.pernix.incae.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class GameCaseInfo {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String title;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_gameCaseId")
    private GameCase gameCase;

    public GameCaseInfo() {
    }

    public GameCaseInfo(GameCaseInfo gameCaseInfo) {
        this.id = gameCaseInfo.getId();
        this.title = gameCaseInfo.getTitle();
        this.content = gameCaseInfo.getContent();
        this.gameCase = gameCaseInfo.getGameCase();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public GameCase getGameCase() {
        return gameCase;
    }

    public void setGameCase(GameCase gameCase) {
        this.gameCase = gameCase;
    }
}
