package com.pernix.incae.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @NotEmpty
    @Column(unique = true, nullable = false)
    private String login;

    @NotEmpty
    @Column(nullable = false)
    private String password;

    @NotNull
    @Column(name = "is_password_lapsed", nullable = false, columnDefinition = "boolean default 'false'")
    private boolean isPasswordLapsed = false;

    @Column(name = "created_at")
    private Date createdAt;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_roleId", nullable = false)
    private Role role;

    @JsonIgnore
    @OneToOne(mappedBy = "user")
    private Game game;

    public Users() {
    }

    public Users(Users user) {
        this.id = user.getId();
        this.name = user.getName();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.role = user.getRole();
        this.isPasswordLapsed  = user.isPasswordLapsed();
        this.createdAt = user.getCreatedAt();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    
    public boolean isPasswordLapsed() {
        return isPasswordLapsed;
    }
    
    public void setIsPasswordLapsed(boolean isPasswordLapsed) {
        this.isPasswordLapsed = isPasswordLapsed;
    }
    
    public Date getCreatedAt() {
        return createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
