package com.pernix.incae.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.jpa.criteria.expression.function.AggregationFunction.MAX;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class TacticScore {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull
    @Column(nullable = false)
    private Double perfect;

    @NotNull
    @Column(nullable = false)
    private Double high;

    @NotNull
    @Column(nullable = false)
    private Double medium;

    @NotNull
    @Column(nullable = false)
    private Double low;

    @NotNull
    @Column(nullable = false)
    private Double negative;

    @NotEmpty
    @Column(length = 500 , nullable = false)
    private String feedbackOptimum;

    @NotEmpty
    @Column(length = 500 , nullable = false)
    private String feedbackNonOptimum;

    @JsonIgnore
    @OneToOne(mappedBy = "tacticScore")
    private Tactic tactic;

    public TacticScore() {
    }

    public TacticScore(TacticScore tacticScore) {
        this.id = tacticScore.getId();
        this.perfect = tacticScore.getPerfect();
        this.high = tacticScore.getHigh();
        this.medium = tacticScore.getMedium();
        this.low = tacticScore.getLow();
        this.negative = tacticScore.getNegative();
        this.feedbackOptimum= tacticScore.getFeedbackOptimum();
        this.feedbackNonOptimum= tacticScore.getFeedbackNonOptimum();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getPerfect() {
        return perfect;
    }

    public void setPerfect(Double perfect) {
        this.perfect = perfect;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }
    
    public Double getMedium() {
        return medium;
    }

    public void setMedium(Double medium) {
        this.medium = medium;
    }
    
    public Double getLow() {
        return medium;
    }

    public void setLow(Double low) {
        this.low = low;
    }
    
    public Double getNegative() {
        return negative;
    }

    public void setNegative(Double negative) {
        this.negative = negative;
    }

    public String getFeedbackOptimum() {
        return feedbackOptimum;
    }

    public void setFeedbackOptimum(String feedbackOptimum) {
        this.feedbackOptimum = feedbackOptimum;
    }

    public String getFeedbackNonOptimum() {
        return feedbackNonOptimum;
    }

    public void setFeedbackNonOptimum(String feedbackNonOptimum) {
        this.feedbackNonOptimum = feedbackNonOptimum;
    }


    public Tactic getTactic() {
        return tactic;
    }

    public void setTactic(Tactic tactic) {
        this.tactic = tactic;
    }

}
