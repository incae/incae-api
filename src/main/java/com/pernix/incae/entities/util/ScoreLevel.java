package com.pernix.incae.entities.util;

public enum ScoreLevel {
    PERFECT, HIGH, MEDIUM, LOW, NEGATIVE, NONE
}