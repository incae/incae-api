package com.pernix.incae.entities.util;

public enum GameStatus {
    IN_PROGRESS, FINISHED_WON, FINISHED_LOST
}