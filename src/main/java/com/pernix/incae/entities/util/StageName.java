package com.pernix.incae.entities.util;

public enum StageName {
    IMPLEMENTAR("Implementar y Medir"), COMUNICAR("Comunicar"), DEFINIR("Definir visión y plan de cambio"), MAPEAR(
            "Mapear el talento"), ANALIZAR("Analizar");

    private final String name;

    private StageName(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
