package com.pernix.incae.entities.util;

public enum TacticClassification {
    MANDATORY, OPTIONAL, NEGATIVE
}