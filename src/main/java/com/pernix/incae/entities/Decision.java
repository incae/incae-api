package com.pernix.incae.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pernix.incae.entities.util.ScoreLevel;
import com.pernix.incae.entities.util.TacticClassification;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Decision {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_gameId", nullable = false)
    private Game game;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_tacticId", nullable = false)
    private Tactic tactic;

    // @NotNull
    @Column(nullable = false)
    private Double gainScore;

    // @NotNull
    @Column(nullable = false)
    private Integer position;

    @Enumerated(EnumType.STRING)
    // @NotNull
    @Column(nullable = false)
    private TacticClassification classification;

    @Enumerated(EnumType.STRING)
    // @NotNull
    @Column(nullable = false)
    private ScoreLevel scoreLevel;

    @NotNull
    private int posX;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "decision_employee", joinColumns = { @JoinColumn(name = "decision_id") }, inverseJoinColumns = {
            @JoinColumn(name = "employeeInfo_id") })
    private Set<EmployeeInfo> selectedEmployees = new HashSet<EmployeeInfo>();

    public Decision() {
    }

    public Decision(Decision decision) {
        this.id = decision.getId();
        this.tactic = decision.getTactic();
        this.game = decision.getGame();
        this.gainScore = decision.getGainScore();
        this.position = decision.getPosition();
        this.classification = decision.getClassification();
        this.scoreLevel = decision.getScoreLevel();
        this.selectedEmployees = decision.getSelectedEmployees();
        this.posX = decision.getPosX();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tactic getTactic() {
        return tactic;
    }

    public void setTactic(Tactic tactic) {
        this.tactic = tactic;
    }

    @JsonIgnore
    public Game getGame() {
        return game;
    }

    @JsonProperty
    public void setGame(Game game) {
        this.game = game;
    }

    public Double getGainScore() {
        return gainScore;
    }

    public void setGainScore(Double gainScore) {
        this.gainScore = gainScore;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public TacticClassification getClassification() {
        return classification;
    }

    public void setClassification(TacticClassification classification) {
        this.classification = classification;
    }
    
    public ScoreLevel getScoreLevel() {
        return scoreLevel;
    }

    public void setScoreLevel(ScoreLevel scoreLevel) {
        this.scoreLevel = scoreLevel;
    }

    public Set<EmployeeInfo> getSelectedEmployees() {
        return selectedEmployees;
    }

    public void setSelectedEmployees(Set<EmployeeInfo> employees) {
        this.selectedEmployees = employees;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }
}
