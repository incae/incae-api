package com.pernix.incae.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Stage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @NotEmpty
    @Column(nullable = false)
    private int minimumDays;

    @NotEmpty
    @Column(nullable = false)
    private int maximumDays;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "stage")
    private Set<Tactic> tactics;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "next_stage")
    private Stage nextStage;

    @JsonIgnore
    @OneToOne(mappedBy = "nextStage")
    private Stage prevStage;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "currentStage")
    private Set<Game> games;

    public Stage() {
    }

    public Stage(Stage stage) {
        this.id = stage.getId();
        this.name = stage.getName();
        this.minimumDays = stage.getMinimumDays();
        this.maximumDays = stage.getMaximumDays();
        this.tactics = stage.getTactics();
        this.nextStage = stage.getNextStage();
        this.games = stage.getGames();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinimumDays() {
        return minimumDays;
    }

    public void setMinimumDays(int minimumDays) {
        this.minimumDays = minimumDays;
    }

    public int getMaximumDays() {
        return maximumDays;
    }

    public void setMaximumDays(int maximumDays) {
        this.maximumDays = maximumDays;
    }

    public Set<Tactic> getTactics() {
        return tactics;
    }

    public void setTactics(Set<Tactic> tactics) {
        this.tactics = tactics;
    }

    public Stage getNextStage() {
        return nextStage;
    }

    public void setNextStage(Stage nextStage) {
        this.nextStage = nextStage;
    }

    public Set<Game> getGames() {
        return games;
    }

    public void setGames(Set<Game> games) {
        this.games = games;
    }

}
