package com.pernix.incae.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pernix.incae.entities.util.GameStatus;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(nullable = false)
    private GameStatus status;

    @NotNull
    @Column(nullable = false)
    private Double culturalChangeScore;

    @NotNull
    @Column(nullable = false)
    private Double leadershipChangeScore;

    @NotNull
    @Column(nullable = false)
    private Double availableDays;

    @NotNull
    @Column(nullable = false)
    private Double budget;

    @NotNull
    @Column(nullable = false)
    private Date lastVisit;

    @NotNull
    @Column(nullable = false)
    private int spentDaysOnCurrentStage;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "game")
    private Set<TeamMember> teamMembers;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "game")
    private Set<Department> departments;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "game")
    private Set<Decision> decisions;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_currentStageId", nullable = false)
    private Stage currentStage;

    @OneToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
    @NotFound(action=NotFoundAction.IGNORE)
    @JoinColumn(name = "FK_userId", referencedColumnName="id")
    private Users user;

    public Game() {
    }

    public Game(Game game) {
        this.id = game.getId();
        this.name = game.getName();
        this.status = game.getStatus();
        this.culturalChangeScore = game.getCulturalChangeScore();
        this.leadershipChangeScore = game.getLeadershipChangeScore();
        this.budget = game.getBudget();
        this.lastVisit = game.getLastVisit();
        this.spentDaysOnCurrentStage = game.getSpentDaysOnCurrentStage();
        this.availableDays = game.getAvailableDays();
        this.teamMembers = game.getTeamMembers();
        this.departments = game.getDepartments();
        this.decisions = game.getDecisions();
        this.currentStage = game.getCurrentStage();
        this.user = game.getUser();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCulturalChangeScore() {
        return culturalChangeScore;
    }

    public void setCulturalChangeScore(Double score) {
        this.culturalChangeScore = score;
    }

    public Double getLeadershipChangeScore() {
        return leadershipChangeScore;
    }

    public void setLeadershipChangeScore(Double leadershipChangeScore) {
        this.leadershipChangeScore = leadershipChangeScore;
    }

    public Double getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(Double availableDays) {
        this.availableDays = availableDays;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Date getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(Date lastVisit) {
        this.lastVisit = lastVisit;
    }

    public int getSpentDaysOnCurrentStage() {
        return spentDaysOnCurrentStage;
    }

    public void setSpentDaysOnCurrentStage(int spentDaysOnCurrentStage) {
        this.spentDaysOnCurrentStage = spentDaysOnCurrentStage;
    }

    public Set<TeamMember> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(Set<TeamMember> teamMembers) {
        this.teamMembers = teamMembers;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    public Set<Decision> getDecisions() {
        return decisions;
    }
    
    public void setDecisions(Set<Decision> decisions) {
        this.decisions = decisions;
    }

    public Stage getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(Stage currentStage) {
        this.currentStage = currentStage;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
