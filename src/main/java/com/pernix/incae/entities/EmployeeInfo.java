package com.pernix.incae.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EmployeeInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @NotEmpty
    @Column(nullable = false)
    private String profilePicture;

    @NotEmpty
    @Column(nullable = false)
    private String jobTitle;

    @NotEmpty
    @Column(nullable = false)
    private int hierarchicalLevel;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "TEXT")
    private String background;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "TEXT")
    private String scenarioDiagnosis;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "TEXT")
    private String proposedSolution;

    @NotNull
    @Column(nullable = false)
    private Double initialCommitmentLevel;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_departmentInfoId", nullable = false)
    private DepartmentInfo departmentInfo;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employeeInfo")
    private Set<Employee> employees;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "selectedEmployees")
    private Set<Decision> decisions;

    public EmployeeInfo() {
    }

    public EmployeeInfo(EmployeeInfo employeeInfo) {
        this.id = employeeInfo.getId();
        this.name = employeeInfo.getName();
        this.profilePicture = employeeInfo.getProfilePicture();
        this.jobTitle = employeeInfo.getJobTitle();
        this.initialCommitmentLevel = employeeInfo.getInitialCommitmentLevel();
        this.hierarchicalLevel = employeeInfo.getHierarchicalLevel();
        this.departmentInfo = employeeInfo.getDepartmentInfo();
        this.employees = employeeInfo.getEmployees();
        // Biography
        this.background = employeeInfo.getBackground();
        this.scenarioDiagnosis = employeeInfo.getScenarioDiagnosis();
        this.proposedSolution = employeeInfo.getProposedSolution();
    }

    public int getHierarchicalLevel() {
        return hierarchicalLevel;
    }

    public void setHierarchicalLevel(int hierarchicalLevel) {
        this.hierarchicalLevel = hierarchicalLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public DepartmentInfo getDepartmentInfo() {
        return departmentInfo;
    }

    public void setDepartmentInfo(DepartmentInfo departmentInfo) {
        this.departmentInfo = departmentInfo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getScenarioDiagnosis() {
        return scenarioDiagnosis;
    }

    public void setScenarioDiagnosis(String scenarioDiagnosis) {
        this.scenarioDiagnosis = scenarioDiagnosis;
    }

    public String getProposedSolution() {
        return proposedSolution;
    }

    public void setProposedSolution(String proposedSolution) {
        this.proposedSolution = proposedSolution;
    }

    public Double getInitialCommitmentLevel() {
        return initialCommitmentLevel;
    }

    public void setInitialCommitmentLevel(Double initialCommitmentLevel) {
        this.initialCommitmentLevel = initialCommitmentLevel;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

}
