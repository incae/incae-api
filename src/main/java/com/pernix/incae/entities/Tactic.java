package com.pernix.incae.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pernix.incae.entities.util.TacticClassification;

@Entity
public class Tactic {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @NotEmpty
    @Column(nullable = false)
    private String description;

    @NotNull
    @Column(nullable = false)
    private Double days;

    @NotNull
    @Column(nullable = false)
    private Double price;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(nullable = false)
    private TacticClassification classification;

    @NotNull
    @Column(nullable = false, unique = false)
    private int idealPosition;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_stageId", nullable = false)
    private Stage stage;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tactic")
    private Set<Decision> decisions;

    @OneToOne
    @JoinColumn(name = "FK_tactic_scoreId", nullable = false)
    private TacticScore tacticScore;

    public Tactic() {
    }

    public Tactic(Tactic tactic) {
        this.id = tactic.getId();
        this.name = tactic.getName();
        this.description = tactic.getDescription();
        this.days = tactic.getDays();
        this.price = tactic.getPrice();
        this.classification = tactic.getClassification();
        this.idealPosition = tactic.getIdealPosition();
        this.tacticScore = tactic.getTacticScore();
        this.stage = tactic.getStage();
        this.decisions = tactic.getDecisions();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDays() {
        return days;
    }

    public void setDays(Double days) {
        this.days = days;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public int getIdealPosition() {
        return idealPosition;
    }

    public void setIdealPosition(int idealPosition) {
        this.idealPosition = idealPosition;
    }

    public Set<Decision> getDecisions() {
        return decisions;
    }

    public void setDecisions(Set<Decision> decisions) {
        this.decisions = decisions;
    }

    public TacticScore getTacticScore() {
        return tacticScore;
    }

    public void setTacticScore(TacticScore tacticScore) {
        this.tacticScore = tacticScore;
    }

    public TacticClassification getClassification() {
        return classification;
    }

    public void setClassification(TacticClassification classification) {
        this.classification = classification;
    }
}
