package com.pernix.incae.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull
    @Column(nullable = false)
    private Double leadershipChangeScore;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_departmentInfoId", nullable = false)
    private DepartmentInfo departmentInfo;

    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_gameId", nullable = false)
    private Game game;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "department")
    private Set<Employee> employees;

    public Department() {
    }

    public Department(Department department) {
        this.id = department.getId();
        this.leadershipChangeScore = department.getLeadershipChangeScore();
        this.departmentInfo = department.getDepartmentInfo();
        this.game = department.getGame();
        this.employees = department.getEmployees();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLeadershipChangeScore() {
        return leadershipChangeScore;
    }

    public void setLeadershipChangeScore(Double leadershipChangeScore) {
        this.leadershipChangeScore = leadershipChangeScore;
    }

    public DepartmentInfo getDepartmentInfo() {
        return departmentInfo;
    }

    public void setDepartmentInfo(DepartmentInfo departmentInfo) {
        this.departmentInfo = departmentInfo;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}
