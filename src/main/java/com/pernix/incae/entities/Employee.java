package com.pernix.incae.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull
    @Column(nullable = false)
    private Double commitmentLevel;

    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_departmentId", nullable = false)
    private Department department;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_employeeInfoId", nullable = false)
    private EmployeeInfo employeeInfo;

    public Employee() {
    }

    public Employee(Employee employee) {
        this.id = employee.getId();
        this.commitmentLevel = employee.getCommitmentLevel();
        this.department = employee.getDepartment();
        this.employeeInfo = employee.getEmployeeInfo();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getCommitmentLevel() {
        return commitmentLevel;
    }

    public void setCommitmentLevel(Double commitmentLevel) {
        this.commitmentLevel = commitmentLevel;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public EmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(EmployeeInfo employeeInfo) {
        this.employeeInfo = employeeInfo;
    }
}
