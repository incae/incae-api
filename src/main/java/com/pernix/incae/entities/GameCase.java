package com.pernix.incae.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class GameCase {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @JsonProperty("casesInfo")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "gameCase")
    @OrderBy("id ASC")
    private Set<GameCaseInfo> gameCaseInfoSet;

    public GameCase() {
    }

    public GameCase(GameCase gameCase) {
        this.id = gameCase.getId();
        this.name = gameCase.getName();
        this.gameCaseInfoSet = gameCase.getGameCaseInfoSet();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<GameCaseInfo> getGameCaseInfoSet() {
        return gameCaseInfoSet;
    }

    public void setGameCaseInfoSet(Set<GameCaseInfo> gameCaseInfoSet) {
        this.gameCaseInfoSet = gameCaseInfoSet;
    }

}
