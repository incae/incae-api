package com.pernix.incae.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.pernix.incae.data.UserRepository;
import com.pernix.incae.entities.Users;

@Component
public class PasswordVerificationTask {
    private static final Logger logger = LoggerFactory.getLogger(PasswordVerificationTask.class);
    private final UserRepository userRepository;
    private final int passwordAvailableDays = 35;
    
    @Autowired
    public PasswordVerificationTask(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @Transactional
    @Scheduled(cron = "0 0 23 * * ?")
    public void verifyPasswordEspiration () throws ParseException {
        logger.info("Verifying password status :: Execution Time - {}", new Date());
        Date currentDate = new Date();
        Iterable<Users> users = userRepository.findAll();
        for (Users user : users) {
            if (user.getCreatedAt() != null) {
                long diffInMillies = Math.abs(currentDate.getTime() - user.getCreatedAt().getTime());
                long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                if (!user.isPasswordLapsed() && diff > passwordAvailableDays) {
                    logger.info(String.format("The password of user '%s' expired", user.getLogin()));
                    user.setIsPasswordLapsed(true);
                }
            }
        }
    }
}
