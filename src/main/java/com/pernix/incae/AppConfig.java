package com.pernix.incae;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Autowired
    Environment env;

    public String getIncaeOauthResouceId() {
        return env.getProperty("incae.oauth2.resource-id");
    }

    public String getIncaeOauthClientId() {
        return env.getProperty("incae.oauth2.client-id");
    }

    public String getIncaeOauthSecret() {
        return env.getProperty("incae.oauth2.secret");
    }

    public Double getIncaeGameAvailableDays() {
        try {
            return new Double(env.getProperty("incae.game.available-days"));
        } catch (NumberFormatException e) {
            return new Double(100.0);
        }
    }

    public Double getIncaeGameBudget() {
        try {
            return new Double(env.getProperty("incae.game.budget"));
        } catch (NumberFormatException e) {
            return new Double(1000000.0);
        }
    }

    public Double getIncaeWinPercentage() {
        return new Double(env.getProperty("incae.game.min-percentage"));
    }
}
