INCAE API

# Summary

Rest services on Spring

# Prerequisites

JDK 8
Maven 3
Postgres 9.5

# Setup

1. Create database user 'incae' with password 'incae'
2. Create database 'incae'
3. Edit application-dev.properties to match Postgres credentials.
4. Edit property `spring.jpa.hibernate.ddl-auto=update` in application-dev.properties to enable the creation of tables in the databases.This option drops/creates tables every time you run the build, so edit it back to the default value once your database is populated.

# Run

Execute the maven command:

##### Windows

SET ENVIROMENT=dev

##### Linux

export ENVIROMENT=dev

###### Run project with test

mvn clean package spring-boot:run

###### Run project without test

mvn spring-boot:run

After the execution the server will run on localhost:3000
